
### Modules ###

import sys


if __name__ == "__main__":

    ### Checking some parameters ###

    if sys.argv > 1:  ## is there an input file?

        if len(sys.argv) > 2:  ## if given a second argument, it will be the name of the generated file.
            ofile = sys.argv[2]
        else:
            ofile = str(sys.argv[1][:sys.argv[1].find(".")])+"_GOstats.list" ## if there is no second argument, it will generate a new file using the input one name.
    else:

        sys.exit("No input file detected")  ## if there is no input file, stop execution

    ### Body of the program ###

    fh1 = open(sys.argv[1], 'r')
    fh2 = open(ofile, 'w')


    gene_dicts = {}

    for line in fh1:
        tmp_array = line.split("\t")
        go_stats = tmp_array[5].split(',')

        if len(go_stats) > 1:
            gene_dicts[tmp_array[0]] = go_stats

    for gene in gene_dicts.keys():
        for GO in gene_dicts[gene]:
            fh2.write("{}\t{}\tDescription\n".format(gene, GO))

