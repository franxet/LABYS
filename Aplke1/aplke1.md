# Aplanochytrium kergulense



## Building DESeq2 parameters




```r
# Set the directory were the HTSeq count files are located.
HTSeq <- "."

# Find the files in the directory for the counts using a common pattern. 
HTSeqFiles <- grep("counts", list.files(HTSeq), value = TRUE)
HTSeqFiles
```

```
## [1] "Aplke1.790ByPlus.nd.counts" "Aplke1.artemia.nd.counts"  
## [3] "Aplke1.serum.nd.counts"     "Aplke1.spartina.nd.counts"
```


```r
# Sample names.
sample <- c("Aplke1.790ByPlus", "Aplke1.artemia", "Aplke1.serum", "Aplke1.spartina")

# Build a sample table.
sampleTable <- data.frame(sampleName=sample, fileName=HTSeqFiles, sample=sample)
```


```r
# Building the DESeqDataSet whcih stores the count data for DESeq2.
dds <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable, directory = HTSeq, design = ~ sample)

# Number of genes.
nrow(dds)
```

```
## [1] 12209
```


```r
# Removing genes with no expressiong in all samples.
dds <- dds[ rowSums(counts(dds)) > 0, ]

# Number of genes after removing the zero expressed ones.
nrow(dds)
```

```
## [1] 11740
```

```r
# Apply DESeq for samples without replicates.
dds <- DESeq(dds)
```

```
## estimating size factors
```

```
## estimating dispersions
```

```
## Warning in checkForExperimentalReplicates(object, modelMatrix): same number of samples and coefficients to fit,
##   estimating dispersion by treating samples as replicates.
##   read the ?DESeq section on 'Experiments without replicates'
```

```
## gene-wise dispersion estimates
```

```
## mean-dispersion relationship
```

```
## final dispersion estimates
```

```
## fitting model and testing
```

```r
# Getting the raw counts (i.e. non-normalized counts)
raw_counts <- counts(dds, normalized = FALSE)
```



## Inspecting the samples

The results below show how both 790ByPlus and Horse Serum have more mean expression that the other two conditions, especially 790ByPlus. 


```r
## Distribution of gene counts with boxplots

# Log-transform to make numbers on scale (+1 to avoid zeroes)
pseudoCount <- log2(as.data.frame(raw_counts + 1)) 

# Melt the data frame
molten.pC <- melt(pseudoCount,
                 variable.name = "Sample",
                 value.name = "value")
```

```
## No id variables; using all as measure variables
```

```r
# Convert cellNr to factor
molten.pC$Sample <- as.factor(molten.pC$Sample) 

# Extracting the first character (substr), convert to factor with labels and levels specified.
molten.pC$Condition <- as.character(factor(substr(molten.pC$Sample, 11, 12), 
                                          levels = c("By", "em", "um", "rt"), labels = c("790ByPlus", "Artemia", "Serum", "Spartina")))

# Boxplot
box <- ggplot(data = molten.pC, aes(x = Sample, y = value, fill = Condition)) +
  geom_boxplot() +
  scale_y_continuous(expand = c(0, 0)) +
  ylab("log2(+1) count") +
  xlab("Sample") +
  theme(axis.text.x = element_text(size = 6),
        axis.text.y = element_text(size = 8),
        axis.title.y = element_text(size = 12),
        axis.title.x = element_text(size = 12))
box
```

<img src="aplke1_files/figure-html/unnamed-chunk-7-1.png" style="display: block; margin: auto;" />

```r
# Same information but in form of a density distribution

ggplot(molten.pC, aes(x = value, colour = Condition, fill = Condition)) + ylim(c(0, 0.2)) +
  geom_density(alpha = 0.2, size = 1.25) + facet_wrap(~ Condition) +
  theme(legend.position = "top") + xlab(expression(log[2](count + 1)))
```

<img src="aplke1_files/figure-html/unnamed-chunk-7-2.png" style="display: block; margin: auto;" />

## DESeq2 results

See below the code and output results for the comparison against 790ByPlus at the top 1000 genes. Code not shown for the other 3 conditions. 


```r
## Results DESeq2 comparing each condition against the others.

# Against 790ByPlus (Bp)
res_ArBp <- results(dds, contrast = c("sample","Aplke1.artemia","Aplke1.790ByPlus"))
res_SeBp <- results(dds, contrast = c("sample","Aplke1.serum","Aplke1.790ByPlus"))
res_SpBp <- results(dds, contrast = c("sample","Aplke1.spartina","Aplke1.790ByPlus"))

# Down-regulated in 790ByPlus
ArBp <- rownames(head(res_ArBp[ order(res_ArBp$log2FoldChange, decreasing = TRUE), ], 1000))
SeBp <- rownames(head(res_SeBp[ order(res_SeBp$log2FoldChange, decreasing = TRUE), ], 1000))
SpBp <- rownames(head(res_SpBp[ order(res_SpBp$log2FoldChange, decreasing = TRUE), ], 1000))

# Up-regulated in 790ByPlus
ArBp_2 <- rownames(head(res_ArBp[ order(res_ArBp$log2FoldChange), ], 1000))
SeBp_2 <- rownames(head(res_SeBp[ order(res_SeBp$log2FoldChange), ], 1000))
SpBp_2 <- rownames(head(res_SpBp[ order(res_SpBp$log2FoldChange), ], 1000))

# Drawing Venn-diagrams for both up and down regulated genes. 
vp <- venn.diagram(list(Ar=ArBp, Se=SeBp, Sp=SpBp), fill = 1:3, alpha=0.3, filename=NULL)
vp_2 <- venn.diagram(list(Ar=ArBp_2, Se=SeBp_2, Sp=SpBp_2), fill = 1:3, alpha=0.3, filename=NULL)

# Plotting them together
grid.arrange(gTree(children = vp), gTree(children = vp_2), ncol=2, left="# Up-regulated genes", right="# Down-regulated genes", top = "Top 1000 genes Up and Down regulated in 790ByPlus")
```

<img src="aplke1_files/figure-html/unnamed-chunk-8-1.png" style="display: block; margin: auto;" />

<img src="aplke1_files/figure-html/unnamed-chunk-9-1.png" style="display: block; margin: auto;" /><img src="aplke1_files/figure-html/unnamed-chunk-9-2.png" style="display: block; margin: auto;" /><img src="aplke1_files/figure-html/unnamed-chunk-9-3.png" style="display: block; margin: auto;" />


* **Regularized log transformation**

First, let's take a look at the genes who are highly expressed across samples. Despite being 790ByPlus and Serum the two samples with more expression, the top 50 highly expressed are mostly in Artemia and Spartina, which are more specific mediums. 


```r
## Transformation

# Regularized log transformation - supposed to be the best for RNAseq data.
rld <- rlog(dds)

## Heatmaps
hmcol <- colorRampPalette(brewer.pal(9, "GnBu"))(100)
hmcol_2 <- colorRampPalette(brewer.pal(9, "YlOrRd"))(100)


# Select the 50 most highly expressed genes (highest mean values across rows) using normalized counts.
select_2 <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:50]
heatmap.2(assay(rld)[select_2, ], col = hmcol,
          Rowv = TRUE, Colv = TRUE, scale=c("row"),
          dendrogram="both", trace="none", margin=c(10, 6), cexRow = 0.7, cexCol = 0.9)
```

<img src="aplke1_files/figure-html/unnamed-chunk-10-1.png" style="display: block; margin: auto;" />

Now, the idea is to take those genes in the intersections from the Venn Diagrams and see their expression patterns in a heatmap. First, the top 50 more expressed genes within the genes of interest (the intersections) and latter the genes for each condition. 


```r
# Getting the union of the intersections
mygenes_1 <- Reduce(intersect, list(ArBp, SeBp, SpBp)) 
mygenes_2 <- Reduce(intersect, list(BpAr, SeAr, SpAr))
mygenes_3 <- Reduce(intersect, list(BpSe, ArSe, SpSe))
mygenes_4 <- Reduce(intersect, list(BpSp, ArSp, SeSp))
mygenes_5 <- Reduce(intersect, list(ArBp_2, SeBp_2, SpBp_2)) 
mygenes_6 <- Reduce(intersect, list(BpAr_2, SeAr_2, SpAr_2))
mygenes_7 <- Reduce(intersect, list(BpSe_2, ArSe_2, SpSe_2))
mygenes_8 <- Reduce(intersect, list(BpSp_2, ArSp_2, SeSp_2))

mygenes <- Reduce(union, list(mygenes_1, mygenes_2, mygenes_3, mygenes_4, mygenes_5, mygenes_6, mygenes_7, mygenes_8))

# Select the 50 most highly expressed genes (highest mean values across rows) of the union using normalized counts.
select <- order(rownames(dds) %in% mygenes, rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:50]
heatmap.2(assay(rld)[select, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-11-1.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of 790ByPlus
select_bp <- order(rownames(dds) %in% union(mygenes_1, mygenes_5), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_bp, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-11-2.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Artemia
select_ar <- order(rownames(dds) %in% union(mygenes_2, mygenes_6), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_ar, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-11-3.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Serum
select_se <- order(rownames(dds) %in% union(mygenes_3, mygenes_7), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_se, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-11-4.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Spartina
select_sp <- order(rownames(dds) %in% union(mygenes_4, mygenes_8), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_sp, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-11-5.png" style="display: block; margin: auto;" />

Apparently, there's a tendency from the conditions to be very different when compared 790ByPlus and Horse Serum to Spartina and Artemia. When clustering by distances of mean expression using all genes, two clusters form: one of 790ByPlus&Serum and another of Spartina&Artemia, which is the same as saying that there's a difference between rich cultures and more specific cultures. 

When clustering with `Hclust`, 790ByPlus and Serum form one group and Artemia and Spartina another. This is very similar to what was shown in the heatmaps. 



```r
# Heatmap of sample-to-sample distances (uses all genes in the sample)
distsRL <- dist(t(assay(rld)))
mat <- as.matrix(distsRL)
heatmap.2(mat, trace="none", col = rev(hmcol_2), margin=c(13, 13), main = "Sample-to-sample distances (rlog)")
```

<img src="aplke1_files/figure-html/unnamed-chunk-12-1.png" style="display: block; margin: auto;" />

```r
# Using the 1000 most highly expressed genes (selected by mean expression across samples):
select <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:1000]
distsRL <- dist(t(assay(rld)[select,]))
hc = hclust(distsRL)
plot(hc, main="Sample-to-sample distances, 1000 top genes (rlog)")
```

<img src="aplke1_files/figure-html/unnamed-chunk-12-2.png" style="display: block; margin: auto;" />

```r
# Selecting genes based on variance (the varFilter in the genefilter package is especially designed for gene data)
m.var <- varFilter(assay(rld), var.func=IQR, var.cutoff=0.6, filterByQuantile=TRUE)
distsRL <- dist(t(m.var))
hc = hclust(distsRL)
plot(hc, main="Sample-to-sample distances, variable genes (rlog)")
```

<img src="aplke1_files/figure-html/unnamed-chunk-12-3.png" style="display: block; margin: auto;" />

* **Using _fake_ replicates**

Considering the results above, another apporach would be to test whether there is differential expression between rich and more specific mediums. The procedure is the exact same, but in this case the adjusted p-values should be more informative. 


```r
# Generating the media conditions
media <- c("rich", "specific", "rich", "specific")

# Build the sample table.
sampleTable_2 <- data.frame(sampleName=sample, fileName=HTSeqFiles, sample=sample, media = media)

# DESeq changing the design to media
dds_2 <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable_2, directory = HTSeq, design = ~ media)

# Number of genes.
nrow(dds_2)
```

```
## [1] 12209
```

```r
# Removing genes with no expressiong in all samples.
dds_2 <- dds_2[ rowSums(counts(dds_2)) > 0, ]

# Number of genes after removing the zero expressed ones.
nrow(dds_2)
```

```
## [1] 11740
```

```r
# Apply DESeq for samples without replicates.
dds_2 <- DESeq(dds_2)
```

```
## estimating size factors
```

```
## estimating dispersions
```

```
## gene-wise dispersion estimates
```

```
## mean-dispersion relationship
```

```
## final dispersion estimates
```

```
## fitting model and testing
```

```r
# Results
res <- results(dds_2)
summary(res)
```

```
## 
## out of 11740 with nonzero total read count
## adjusted p-value < 0.1
## LFC > 0 (up)     : 744, 6.3% 
## LFC < 0 (down)   : 392, 3.3% 
## outliers [1]     : 0, 0% 
## low counts [2]   : 456, 3.9% 
## (mean count < 8)
## [1] see 'cooksCutoff' argument of ?results
## [2] see 'independentFiltering' argument of ?results
```

Considering the type of medium, adjusted p-values appear. Now, the idea is to compare these genes with the ones obtained from doing the qualitative assessment on conditions.

First, let's look at those genes that are both DEgenes and are in the top highly expressed genes from the other approach. Secondly, just look at the DEgenes and their expression patterns.


```r
# The DE genes, 744 up regulated and 394 down regulated, 1138 in total.
DEgenes <- rownames(head(res[ order(res$padj), ], 1138))

# Number of singificant DEgenes that are also found in the qualitative assesment. 
intgenes <- intersect(DEgenes, mygenes)
length(intgenes)
```

[1] 240

```r
# Look at the genes that are in both set of genes and plot them
select_3 <- order(rownames(dds) %in% intgenes, rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:77]
heatmap.2(assay(rld)[select_3, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.6, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-14-1.png" style="display: block; margin: auto;" />

```r
# Let's select just all DEgenes and look at their expression patterns
select_4 <- order(rownames(dds) %in% DEgenes, rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:1138]
heatmap.2(assay(rld)[select_4, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.1, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-14-2.png" style="display: block; margin: auto;" />

These results are expected: by the qualitative assessment we are recovering those genes that are highly or lower expressed in **one** condition. However, by building 'fake' replicates, we are observing those genes that are expressed in two different types of cultures. In order to be compared, it is needed to have the intersection of just Artemia&Spartina (or 790ByPlus&Serum). 


```r
# Intersections of Artemia and Spartina
mygenes_11 <- Reduce(intersect, list(ArBp, SpBp)) # Up-regulated in 790BP
mygenes_33 <- Reduce(intersect, list(ArSe, SpSe)) # Up-regulated in Serum 
mygenes_55 <- Reduce(intersect, list(ArBp_2, SpBp_2)) # Down-regulated in 790BP
mygenes_77 <- Reduce(intersect, list(ArSe_2, SpSe_2)) # Down-regulated in Serum

mygenes_ArSp <- Reduce(union, list(mygenes_11, mygenes_33, mygenes_55, mygenes_77))
length(mygenes_ArSp)
```

[1] 1863

```r
# How many genes shared between approaches
intgenes_2 <- intersect(mygenes_ArSp, DEgenes)
length(intgenes_2)
```

[1] 743

```r
# Heatmap of the intgenes_2
select_5 <- order(rownames(dds) %in% intgenes_2, rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:813]
heatmap.2(assay(rld)[select_5, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.1, cexCol = 0.7)
```

<img src="aplke1_files/figure-html/unnamed-chunk-15-1.png" style="display: block; margin: auto;" />

The conclusion of this comparison is that it is safe to use the 'fake' replicates, because 71.4% of the DE genes are present also in the genes of interest from the 'correct' approach. 

## Functional analyses 

* **'Rich' vs 'Specific' medium**

Next step is to analyze the functionality of these DE genes. Becuase these genes are already annotated, the protein IDs can be used to extract the information from the catalogs. The DE genes analysed will be the ones extracted from the results tables using fake replicates.


```r
# Genes of interest
upreg <- rownames(res)[which(res$padj < 0.01 & res$log2FoldChange > 0)]
downreg <- rownames(res)[which(res$padj < 0.01 & res$log2FoldChange < 0)]

# Write them on a file
write.table(as.character(upreg), file = "upreg.txt", sep = "\t")
write.table(as.character(downreg), file = "downreg.txt", sep = "\t")
```

Once the vectors are on files, this can be compared to the gtf file to extract the Protein IDs. With the protIDs, the original aminoacid sequences can be compared against different databases. 


```bash
# Remove the headers
cut -f2 upreg.txt | grep -v "x" > upreg.list
cut -f2 downreg.txt | grep -v "x" > downreg.list

# Get the upregulated protein IDs
cat upreg.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_protID.list
wc -l upreg_protID.list

# Get the downregulated protein IDs
cat downreg.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_protID.list
wc -l downreg_protID.list
```

Use the annotated catalog of GO terms and see the **Biological process** (BP), **Molecular function** (MF) or **Cellular component** (CC) for desired proteins.


```bash
# Get the GO annotation from protein list

# Up-regulated genes
cat upreg_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "biological_process" | cut -f3 | sort | uniq -c > upreg_GO_BP.list

cat upreg_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "molecular_function" | cut -f3 | sort | uniq -c > upreg_GO_MF.list

cat upreg_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "cellular_component" | cut -f3 | sort | uniq -c > upreg_GO_CC.list

# Down-regulated genes
cat downreg_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "biological_process" | cut -f3 | sort | uniq -c > downreg_GO_BP.list

cat downreg_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "molecular_function" | cut -f3 | sort | uniq -c > downreg_GO_MF.list

cat downreg_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "cellular_component" | cut -f3 | sort | uniq -c > downreg_GO_CC.list
```

Results do not show any specific information about the up-regulated or down-regulated genes (see gitlab folder). GO annotation points to the up-regulation of apoptotic pathways. Generaly speaking, using the specific vs rich approach does not yield any substantial results.

- Enrichment Analysis 


```bash

# First, parse the eggNOG-mapper annotations file to the tabular format GOstatsPlus needs
python ../convertGOstatsPlus.py Aplke1_GeneCatalog_proteins_20121220_one_line.fasta.emapper.annotations Aplke1_GeneCatalog_proteins_20121220.gostats

# Second, change the names to the protein IDs
awk 'BEGIN {OFS="\t"};  { split($1, a, "|"); print a[3], $2, $3 }' Aplke1_GeneCatalog_proteins_20121220.gostats > Aplke1_GeneCatalog_proteins_20121220.annot

```

After the annot file is done, the GOstatsPlus package can be used (GOstats, GOstatsPlus and GSEABase libraries are loaded at the beginning of the file).


```r
# Create the GOstats object and save it, in case that we want to re-run the sript. These are all the annotations available for Aplke1 proteins.
if(file.exists("Aplke1_gsc.rda")){
  load(file="Aplke1_gsc.rda")
}else{
  fn_annot = "Aplke1_GeneCatalog_proteins_20121220.annot"
  GO_gsc = b2g_to_gsc(file = fn_annot, organism = "Aplke1")
  save(GO_gsc, file = "Aplke1_gsc.rda")
}

# We need a vector with all the genes of interest. Important that the vectors are characters, since the .annot file is parsed as a string and not as an integer.
up_tbl <- read.table(file = "upreg_protID.list")
down_tbl <- read.table(file = "downreg_protID.list")

up_IDs <- as.character(up_tbl$V1)
down_IDs <- as.character(down_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
up_BP <- test_GO(up_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.01)
up_MF <- test_GO(up_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.01)
up_CC <- test_GO(up_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.01)

down_BP <- test_GO(down_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.01)
down_MF <- test_GO(down_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.01)
down_CC <- test_GO(down_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.01)
```

Let's see the 20 more significant Molecular functions found using this method. First the up-regulated functions and secondly, the down-regulated ones.


```r
# Focusing on the top 20 more significant of Molecular functions

head(summary(up_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio   ExpCount Count Size
## 1  GO:0030898 2.086522e-05 32.585859 0.17492355     4   22
## 2  GO:0043621 1.906857e-04 35.130435 0.11926606     3   15
## 3  GO:0030695 8.305089e-04  8.157350 0.77125382     5   97
## 4  GO:0004311 1.246008e-03 53.983333 0.05565749     2    7
## 5  GO:0008017 1.444118e-03  9.648485 0.50886850     4   64
## 6  GO:0005509 1.530514e-03  9.487332 0.51681957     4   65
## 7  GO:0060589 1.658463e-03  6.913580 0.89847095     5  113
## 8  GO:0015101 2.115193e-03 38.535714 0.07155963     2    9
## 9  GO:0008519 2.115193e-03 38.535714 0.07155963     2    9
## 10 GO:0001071 3.449259e-03  7.478158 0.64403670     4   81
## 11 GO:0003700 3.449259e-03  7.478158 0.64403670     4   81
## 12 GO:0015631 3.606585e-03  7.379953 0.65198777     4   82
## 13 GO:0005516 4.172113e-03 10.719064 0.33394495     3   42
## 14 GO:0005088 5.217578e-03 22.444444 0.11131498     2   14
## 15 GO:0043531 5.739762e-03  9.486166 0.37370031     3   47
## 16 GO:0004659 5.990946e-03 20.711538 0.11926606     2   15
## 17 GO:0001076 6.813445e-03 19.226190 0.12721713     2   16
## 18 GO:0017034 7.951070e-03       Inf 0.00795107     1    1
## 19 GO:0004310 7.951070e-03       Inf 0.00795107     1    1
## 20 GO:0051996 7.951070e-03       Inf 0.00795107     1    1
##                                                                             Term
## 1                                                actin-dependent ATPase activity
## 2                                                       protein self-association
## 3                                                      GTPase regulator activity
## 4                                              farnesyltranstransferase activity
## 5                                                            microtubule binding
## 6                                                            calcium ion binding
## 7                                   nucleoside-triphosphatase regulator activity
## 8                              organic cation transmembrane transporter activity
## 9                                    ammonium transmembrane transporter activity
## 10                            nucleic acid binding transcription factor activity
## 11                  transcription factor activity, sequence-specific DNA binding
## 12                                                               tubulin binding
## 13                                                            calmodulin binding
## 14                                Ras guanyl-nucleotide exchange factor activity
## 15                                                                   ADP binding
## 16                                                    prenyltransferase activity
## 17 transcription factor activity, RNA polymerase II transcription factor binding
## 18                                Rap guanyl-nucleotide exchange factor activity
## 19                             farnesyl-diphosphate farnesyltransferase activity
## 20                                                    squalene synthase activity
```

```r
head(summary(down_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0016491 1.714179e-06  10.12698 2.207951070    11  361
## 2  GO:0004774 3.554847e-05       Inf 0.012232416     2    2
## 3  GO:0004775 3.554847e-05       Inf 0.012232416     2    2
## 4  GO:0016616 1.072599e-04  13.55556 0.507645260     5   83
## 5  GO:0016614 2.043000e-04  11.70370 0.581039755     5   95
## 6  GO:0016405 2.117275e-04 180.44444 0.024464832     2    4
## 7  GO:0016620 2.799442e-04  30.00929 0.134556575     3   22
## 8  GO:0016903 6.463046e-04  21.88235 0.177370031     3   29
## 9  GO:0016878 7.329185e-04  72.11111 0.042813456     2    7
## 10 GO:0004029 9.736405e-04  60.07407 0.048929664     2    8
## 11 GO:0033764 1.247233e-03  51.47619 0.055045872     2    9
## 12 GO:0016229 1.891552e-03  40.01235 0.067278287     2   11
## 13 GO:0048029 3.558525e-03  27.66667 0.091743119     2   15
## 14 GO:0042132 6.116208e-03       Inf 0.006116208     1    1
## 15 GO:0016155 6.116208e-03       Inf 0.006116208     1    1
## 16 GO:0004347 6.116208e-03       Inf 0.006116208     1    1
## 17 GO:0004331 6.116208e-03       Inf 0.006116208     1    1
## 18 GO:0047035 6.116208e-03       Inf 0.006116208     1    1
## 19 GO:0004776 6.116208e-03       Inf 0.006116208     1    1
## 20 GO:0004616 6.116208e-03       Inf 0.006116208     1    1
##                                                                                               Term
## 1                                                                          oxidoreductase activity
## 2                                                                    succinate-CoA ligase activity
## 3                                                      succinate-CoA ligase (ADP-forming) activity
## 4            oxidoreductase activity, acting on the CH-OH group of donors, NAD or NADP as acceptor
## 5                                         oxidoreductase activity, acting on CH-OH group of donors
## 6                                                                              CoA-ligase activity
## 7  oxidoreductase activity, acting on the aldehyde or oxo group of donors, NAD or NADP as acceptor
## 8                           oxidoreductase activity, acting on the aldehyde or oxo group of donors
## 9                                                                       acid-thiol ligase activity
## 10                                                           aldehyde dehydrogenase (NAD) activity
## 11    steroid dehydrogenase activity, acting on the CH-OH group of donors, NAD or NADP as acceptor
## 12                                                                  steroid dehydrogenase activity
## 13                                                                          monosaccharide binding
## 14                                                fructose 1,6-bisphosphate 1-phosphatase activity
## 15                                                   formyltetrahydrofolate dehydrogenase activity
## 16                                                          glucose-6-phosphate isomerase activity
## 17                                                fructose-2,6-bisphosphate 2-phosphatase activity
## 18                                                      testosterone dehydrogenase (NAD+) activity
## 19                                                     succinate-CoA ligase (GDP-forming) activity
## 20                                       phosphogluconate dehydrogenase (decarboxylating) activity
```

The results show how in Spartina and Artemia, cell growth is up-regulated while the processing of fatty acids is down-regulated. 

- **Top expressed genes by condition**

This results are taking into consideration the clustering of mediums. The same analysis can be made, but instead of using the 'fake' replicates approach, the up and down regulated genes in each condition separately. 

* 790ByPlus 


```r
# Write genes on a file
write.table(as.character(mygenes_1), file = "upreg_790BP.txt", sep = "\t")
write.table(as.character(mygenes_5), file = "downreg_790BP.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_790BP.txt | grep -v "x" > upreg_790BP.list
cut -f2 downreg_790BP.txt | grep -v "x" > downreg_790BP.list

# Get the upregulated protein IDs
cat upreg_790BP.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_790BP_protID.list
wc -l upreg_790BP_protID.list

# Get the downregulated protein IDs
cat downreg_790BP.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_790BP_protID.list
wc -l downreg_790BP_protID.list
```


```r
upBP_tbl <- read.table(file = "upreg_790BP_protID.list")
downBP_tbl <- read.table(file = "downreg_790BP_protID.list")

upBP_IDs <- as.character(upBP_tbl$V1)
downBP_IDs <- as.character(downBP_tbl$V1)
```



```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upBP_BP <- test_GO(upBP_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upBP_MF <- test_GO(upBP_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upBP_CC <- test_GO(upBP_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downBP_BP <- test_GO(downBP_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downBP_MF <- test_GO(downBP_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downBP_CC <- test_GO(downBP_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upBP_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio   ExpCount Count Size
## 1  GO:0015101 0.0001569329 41.307692 0.11559633     3    9
## 2  GO:0008519 0.0001569329 41.307692 0.11559633     3    9
## 3  GO:0004467 0.0009508409 80.650000 0.05137615     2    4
## 4  GO:0031957 0.0009508409 80.650000 0.05137615     2    4
## 5  GO:0031683 0.0029189432 12.338462 0.29541284     3   23
## 6  GO:0001664 0.0029189432 12.338462 0.29541284     3   23
## 7  GO:0030295 0.0033062922 11.747253 0.30825688     3   24
## 8  GO:0008047 0.0039887556  4.511594 1.55412844     6  121
## 9  GO:0019209 0.0051639826  9.855385 0.35963303     3   28
## 10 GO:0015645 0.0054767744 23.007143 0.11559633     2    9
## 11 GO:0032403 0.0058960236  4.137333 1.68256881     6  131
## 12 GO:0022890 0.0067640664  4.711712 1.22018349     5   95
## 13 GO:0016905 0.0114863475 14.622727 0.16697248     2   13
## 14 GO:0005215 0.0115333295  2.637061 4.52110092    10  352
## 15 GO:0004473 0.0128440367       Inf 0.01284404     1    1
## 16 GO:0000035 0.0128440367       Inf 0.01284404     1    1
## 17 GO:0015398 0.0128440367       Inf 0.01284404     1    1
## 18 GO:0008509 0.0141261906  4.748872 0.95045872     4   74
## 19 GO:0022892 0.0205684705  2.630855 3.50642202     8  273
## 20 GO:0008324 0.0206304166  3.500000 1.60550459     5  125
##                                                                          Term
## 1                           organic cation transmembrane transporter activity
## 2                                 ammonium transmembrane transporter activity
## 3                                   long-chain fatty acid-CoA ligase activity
## 4                              very long-chain fatty acid-CoA ligase activity
## 5                                G-protein beta/gamma-subunit complex binding
## 6                                          G-protein coupled receptor binding
## 7                                           protein kinase activator activity
## 8                                                   enzyme activator activity
## 9                                                   kinase activator activity
## 10                                                 fatty acid ligase activity
## 11                                                    protein complex binding
## 12                        inorganic cation transmembrane transporter activity
## 13                                         myosin heavy chain kinase activity
## 14                                                       transporter activity
## 15                    malate dehydrogenase (decarboxylating) (NADP+) activity
## 16                                                               acyl binding
## 17 high-affinity secondary active ammonium transmembrane transporter activity
## 18                                   anion transmembrane transporter activity
## 19                                    substrate-specific transporter activity
## 20                                  cation transmembrane transporter activity
```

```r
head(summary(downBP_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio   ExpCount Count Size
## 1  GO:0003960 0.0006589771 97.969697 0.04281346     2    4
## 2  GO:0003857 0.0038228784 27.948052 0.09633028     2    9
## 3  GO:0004325 0.0107033639       Inf 0.01070336     1    1
## 4  GO:0003871 0.0107033639       Inf 0.01070336     1    1
## 5  GO:0008834 0.0107033639       Inf 0.01070336     1    1
## 6  GO:0030350 0.0107033639       Inf 0.01070336     1    1
## 7  GO:0004147 0.0107033639       Inf 0.01070336     1    1
## 8  GO:0043723 0.0107033639       Inf 0.01070336     1    1
## 9  GO:0042085 0.0107033639       Inf 0.01070336     1    1
## 10 GO:0008098 0.0107033639       Inf 0.01070336     1    1
## 11 GO:0022838 0.0110216590  7.303354 0.47094801     3   44
## 12 GO:0016616 0.0111028397  5.154757 0.88837920     4   83
## 13 GO:0016491 0.0114155077  2.835118 3.86391437     9  361
## 14 GO:0015267 0.0132023124  6.799006 0.50305810     3   47
## 15 GO:0022803 0.0132023124  6.799006 0.50305810     3   47
## 16 GO:0016655 0.0136871794 13.010101 0.18195719     2   17
## 17 GO:0046906 0.0152954007 12.193182 0.19266055     2   18
## 18 GO:0020037 0.0152954007 12.193182 0.19266055     2   18
## 19 GO:0016614 0.0175672289  4.457994 1.01681957     4   95
## 20 GO:0016651 0.0211395485  5.628538 0.59938838     3   56
##                                                                                     Term
## 1                                                       NADPH:quinone reductase activity
## 2                                               3-hydroxyacyl-CoA dehydrogenase activity
## 3                                                                ferrochelatase activity
## 4        5-methyltetrahydropteroyltriglutamate-homocysteine S-methyltransferase activity
## 5                                    di-trans,poly-cis-decaprenylcistransferase activity
## 6                                                        iron-responsive element binding
## 7                               dihydrolipoamide branched chain acyltransferase activity
## 8          2,5-diamino-6-ribitylamino-4(3H)-pyrimidinone 5'-phosphate deaminase activity
## 9          5-methyltetrahydropteroyltri-L-glutamate-dependent methyltransferase activity
## 10                                                    5S rRNA primary transcript binding
## 11                                                   substrate-specific channel activity
## 12 oxidoreductase activity, acting on the CH-OH group of donors, NAD or NADP as acceptor
## 13                                                               oxidoreductase activity
## 14                                                                      channel activity
## 15                                            passive transmembrane transporter activity
## 16   oxidoreductase activity, acting on NAD(P)H, quinone or similar compound as acceptor
## 17                                                                  tetrapyrrole binding
## 18                                                                          heme binding
## 19                              oxidoreductase activity, acting on CH-OH group of donors
## 20                                            oxidoreductase activity, acting on NAD(P)H
```

Interesting enough, in the best conditions (790ByPlus), Aplke1 seems to have up-regulated some genes involved in the degradation of fatty acids, while utilization of other energy sources seem to be somehow downregulated when compared to the rest of conditions.

* Artemia


```r
# Write genes on a file
write.table(as.character(mygenes_2), file = "upreg_Ar.txt", sep = "\t")
write.table(as.character(mygenes_6), file = "downreg_Ar.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Ar.txt | grep -v "x" > upreg_Ar.list
cut -f2 downreg_Ar.txt | grep -v "x" > downreg_Ar.list

# Get the upregulated protein IDs
cat upreg_Ar.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Ar_protID.list
wc -l upreg_Ar_protID.list

# Get the downregulated protein IDs
cat downreg_Ar.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Ar_protID.list
wc -l downreg_Ar_protID.list
```


```r
upAr_tbl <- read.table(file = "upreg_Ar_protID.list")
downAr_tbl <- read.table(file = "downreg_Ar_protID.list")

upAr_IDs <- as.character(upAr_tbl$V1)
downAr_IDs <- as.character(downAr_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upAr_BP <- test_GO(upAr_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upAr_MF <- test_GO(upAr_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upAr_CC <- test_GO(upAr_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downAr_BP <- test_GO(downAr_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downAr_MF <- test_GO(downAr_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downAr_CC <- test_GO(downAr_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upAr_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio   ExpCount Count Size
## 1  GO:0036310 0.0007776313 89.722222 0.04648318     2    4
## 2  GO:0097617 0.0012865473 59.796296 0.05810398     2    5
## 3  GO:0008559 0.0080659602 17.900000 0.13944954     2   12
## 4  GO:0042910 0.0080659602 17.900000 0.13944954     2   12
## 5  GO:0042626 0.0086309779  5.557507 0.82507645     4   71
## 6  GO:0016820 0.0086309779  5.557507 0.82507645     4   71
## 7  GO:0043492 0.0095094790  5.393009 0.84831804     4   73
## 8  GO:0008127 0.0116207951       Inf 0.01162080     1    1
## 9  GO:0008915 0.0116207951       Inf 0.01162080     1    1
## 10 GO:0000170 0.0116207951       Inf 0.01162080     1    1
## 11 GO:0015238 0.0142427294 12.769841 0.18593272     2   16
## 12 GO:0004364 0.0142427294 12.769841 0.18593272     2   16
## 13 GO:0050660 0.0146835692  6.510204 0.52293578     3   45
## 14 GO:0015399 0.0153901183  4.635294 0.97614679     4   84
## 15 GO:0015405 0.0153901183  4.635294 0.97614679     4   84
## 16 GO:0090484 0.0160243954 11.914815 0.19755352     2   17
## 17 GO:0009703 0.0231100609 87.324324 0.02324159     1    2
## 18 GO:0015232 0.0231100609 87.324324 0.02324159     1    2
## 19 GO:0004396 0.0231100609 87.324324 0.02324159     1    2
## 20 GO:0070530 0.0231100609 87.324324 0.02324159     1    2
##                                                                                              Term
## 1                                                                     annealing helicase activity
## 2                                                                              annealing activity
## 3                                                         xenobiotic-transporting ATPase activity
## 4                                                                 xenobiotic transporter activity
## 5                                ATPase activity, coupled to transmembrane movement of substances
## 6  hydrolase activity, acting on acid anhydrides, catalyzing transmembrane movement of substances
## 7                                              ATPase activity, coupled to movement of substances
## 8                                                              quercetin 2,3-dioxygenase activity
## 9                                                          lipid-A-disaccharide synthase activity
## 10                                                               sphingosine hydroxylase activity
## 11                                                        drug transmembrane transporter activity
## 12                                                               glutathione transferase activity
## 13                                                            flavin adenine dinucleotide binding
## 14                                              primary active transmembrane transporter activity
## 15                                  P-P-bond-hydrolysis-driven transmembrane transporter activity
## 16                                                                      drug transporter activity
## 17                                                              nitrate reductase (NADH) activity
## 18                                                                      heme transporter activity
## 19                                                                            hexokinase activity
## 20                                                               K63-linked polyubiquitin binding
```

```r
head(summary(downAr_MF), 20)
```

```
##        GOMFID       Pvalue  OddsRatio   ExpCount Count Size
## 1  GO:0030547 5.163883e-05        Inf 0.01467890     2    2
## 2  GO:0030156 5.163883e-05        Inf 0.01467890     2    2
## 3  GO:0030545 1.542212e-04 295.000000 0.02201835     2    3
## 4  GO:0031210 3.070587e-04 147.454545 0.02935780     2    4
## 5  GO:0050997 5.094692e-04  98.272727 0.03669725     2    5
## 6  GO:0070405 5.094692e-04  98.272727 0.03669725     2    5
## 7  GO:0005319 1.640988e-03  15.314286 0.24220183     3   33
## 8  GO:0008289 1.732266e-03   9.208696 0.53577982     4   73
## 9  GO:0043178 1.801434e-03  42.064935 0.06605505     2    9
## 10 GO:0015245 2.727605e-03  32.696970 0.08073394     2   11
## 11 GO:0005324 2.727605e-03  32.696970 0.08073394     2   11
## 12 GO:0000062 3.258481e-03  29.418182 0.08807339     2   12
## 13 GO:0005543 5.130367e-03   9.937888 0.35963303     3   49
## 14 GO:0001078 7.339450e-03        Inf 0.00733945     1    1
## 15 GO:0004310 7.339450e-03        Inf 0.00733945     1    1
## 16 GO:0051996 7.339450e-03        Inf 0.00733945     1    1
## 17 GO:0004336 7.339450e-03        Inf 0.00733945     1    1
## 18 GO:0001227 7.339450e-03        Inf 0.00733945     1    1
## 19 GO:0070336 7.339450e-03        Inf 0.00733945     1    1
## 20 GO:1901681 1.397451e-02  12.739130 0.18348624     2   25
##                                                                                                               Term
## 1                                                                                      receptor inhibitor activity
## 2                                                                                  benzodiazepine receptor binding
## 3                                                                                      receptor regulator activity
## 4                                                                                      phosphatidylcholine binding
## 5                                                                                quaternary ammonium group binding
## 6                                                                                             ammonium ion binding
## 7                                                                                       lipid transporter activity
## 8                                                                                                    lipid binding
## 9                                                                                                  alcohol binding
## 10                                                                                 fatty acid transporter activity
## 11                                                                      long-chain fatty acid transporter activity
## 12                                                                                          fatty-acyl-CoA binding
## 13                                                                                            phospholipid binding
## 14   transcriptional repressor activity, RNA polymerase II core promoter proximal region sequence-specific binding
## 15                                                               farnesyl-diphosphate farnesyltransferase activity
## 16                                                                                      squalene synthase activity
## 17                                                                                   galactosylceramidase activity
## 18 transcriptional repressor activity, RNA polymerase II transcription regulatory region sequence-specific binding
## 19                                                                                     flap-structured DNA binding
## 20                                                                                         sulfur compound binding
```

Fatty acids seem to be down-regulated while the transportation of xenobiotic substances are up-regulated. 

* Serum 


```r
# Write genes on a file
write.table(as.character(mygenes_3), file = "upreg_Se.txt", sep = "\t")
write.table(as.character(mygenes_7), file = "downreg_Se.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Se.txt | grep -v "x" > upreg_Se.list
cut -f2 downreg_Se.txt | grep -v "x" > downreg_Se.list

# Get the upregulated protein IDs
cat upreg_Se.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Se_protID.list
wc -l upreg_Se_protID.list

# Get the downregulated protein IDs
cat downreg_Se.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Se_protID.list
wc -l downreg_Se_protID.list
```


```r
upSe_tbl <- read.table(file = "upreg_Se_protID.list")
downSe_tbl <- read.table(file = "downreg_Se_protID.list")

upSe_IDs <- as.character(upSe_tbl$V1)
downSe_IDs <- as.character(downSe_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upSe_BP <- test_GO(upSe_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upSe_MF <- test_GO(upSe_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upSe_CC <- test_GO(upSe_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downSe_BP <- test_GO(downSe_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downSe_MF <- test_GO(downSe_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downSe_CC <- test_GO(downSe_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```

```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upSe_MF), 20)
```

```
##        GOMFID     Pvalue OddsRatio   ExpCount Count Size
## 1  GO:1990939 0.00282584  33.64583 0.08318043     2    8
## 2  GO:0008574 0.00282584  33.64583 0.08318043     2    8
## 3  GO:0045303 0.01039755       Inf 0.01039755     1    1
## 4  GO:0004104 0.01039755       Inf 0.01039755     1    1
## 5  GO:0003990 0.01039755       Inf 0.01039755     1    1
## 6  GO:0003989 0.01039755       Inf 0.01039755     1    1
## 7  GO:0000171 0.01039755       Inf 0.01039755     1    1
## 8  GO:0008398 0.01039755       Inf 0.01039755     1    1
## 9  GO:0003777 0.01446832  12.57812 0.18715596     2   18
## 10 GO:0008483 0.01773571  11.17361 0.20795107     2   20
## 11 GO:0015186 0.02069015  98.03030 0.02079511     1    2
## 12 GO:0015188 0.02069015  98.03030 0.02079511     1    2
## 13 GO:0015114 0.02069015  98.03030 0.02079511     1    2
## 14 GO:0005436 0.02069015  98.03030 0.02079511     1    2
## 15 GO:0004075 0.02069015  98.03030 0.02079511     1    2
## 16 GO:0015658 0.02069015  98.03030 0.02079511     1    2
## 17 GO:0004421 0.02069015  98.03030 0.02079511     1    2
## 18 GO:0004146 0.02069015  98.03030 0.02079511     1    2
## 19 GO:0045703 0.02069015  98.03030 0.02079511     1    2
## 20 GO:0009374 0.02069015  98.03030 0.02079511     1    2
##                                                            Term
## 1                      ATP-dependent microtubule motor activity
## 2   ATP-dependent microtubule motor activity, plus-end-directed
## 3          diaminobutyrate-2-oxoglutarate transaminase activity
## 4                                       cholinesterase activity
## 5                                 acetylcholinesterase activity
## 6                               acetyl-CoA carboxylase activity
## 7                                     ribonuclease MRP activity
## 8                                sterol 14-demethylase activity
## 9                                    microtubule motor activity
## 10                                        transaminase activity
## 11               L-glutamine transmembrane transporter activity
## 12              L-isoleucine transmembrane transporter activity
## 13             phosphate ion transmembrane transporter activity
## 14                          sodium:phosphate symporter activity
## 15                                  biotin carboxylase activity
## 16 branched-chain amino acid transmembrane transporter activity
## 17                  hydroxymethylglutaryl-CoA synthase activity
## 18                             dihydrofolate reductase activity
## 19                                       ketoreductase activity
## 20                                               biotin binding
```

```r
head(summary(downSe_MF), 20)
```

```
##        GOMFID       Pvalue  OddsRatio   ExpCount Count Size
## 1  GO:0008017 4.882952e-08  13.308081 1.05688073    10   64
## 2  GO:0015631 5.508896e-07   9.924242 1.35412844    10   82
## 3  GO:0008092 2.546964e-06   5.669251 3.31926606    14  201
## 4  GO:0004497 1.132294e-05  11.255319 0.80917431     7   49
## 5  GO:0008395 1.684122e-05 189.117647 0.06605505     3    4
## 6  GO:0051010 4.161037e-05  94.529412 0.08256881     3    5
## 7  GO:1990939 2.249422e-04  37.776471 0.13211009     3    8
## 8  GO:0008574 2.249422e-04  37.776471 0.13211009     3    8
## 9  GO:0003774 5.039555e-04   6.927632 1.04036697     6   63
## 10 GO:0004423 7.946880e-04 123.653846 0.04954128     2    3
## 11 GO:0016709 8.798535e-04  11.106087 0.44587156     4   27
## 12 GO:0005088 1.362777e-03  17.139037 0.23119266     3   14
## 13 GO:0003777 2.915408e-03  12.552941 0.29724771     3   18
## 14 GO:0030676 3.848603e-03  30.884615 0.09908257     2    6
## 15 GO:0016705 5.130079e-03   5.025510 1.13944954     5   69
## 16 GO:0005089 7.033211e-03  20.576923 0.13211009     2    8
## 17 GO:0000254 8.947421e-03  17.631868 0.14862385     2    9
## 18 GO:0016491 1.263835e-02   2.347114 5.96146789    12  361
## 19 GO:0001671 1.589171e-02  12.330769 0.19816514     2   12
## 20 GO:0050479 1.651376e-02        Inf 0.01651376     1    1
##                                                                                                                                                                    Term
## 1                                                                                                                                                   microtubule binding
## 2                                                                                                                                                       tubulin binding
## 3                                                                                                                                          cytoskeletal protein binding
## 4                                                                                                                                                monooxygenase activity
## 5                                                                                                                                          steroid hydroxylase activity
## 6                                                                                                                                          microtubule plus-end binding
## 7                                                                                                                              ATP-dependent microtubule motor activity
## 8                                                                                                           ATP-dependent microtubule motor activity, plus-end-directed
## 9                                                                                                                                                        motor activity
## 10                                                                                                                                       iduronate-2-sulfatase activity
## 11 oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, NAD(P)H as one donor, and incorporation of one atom of oxygen
## 12                                                                                                                       Ras guanyl-nucleotide exchange factor activity
## 13                                                                                                                                           microtubule motor activity
## 14                                                                                                                       Rac guanyl-nucleotide exchange factor activity
## 15                                                                oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen
## 16                                                                                                                       Rho guanyl-nucleotide exchange factor activity
## 17                                                                                                                                    C-4 methylsterol oxidase activity
## 18                                                                                                                                              oxidoreductase activity
## 19                                                                                                                                            ATPase activator activity
## 20                                                                                                                                glyceryl-ether monooxygenase activity
```

In serum, transport of foreign amino acids, while some enzymes involved in the production of ATP seem to be down-regulated or less expressed that in other conditions. 


* Spartina


```r
# Write genes on a file
write.table(as.character(mygenes_4), file = "upreg_Sp.txt", sep = "\t")
write.table(as.character(mygenes_8), file = "downreg_Sp.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Sp.txt | grep -v "x" > upreg_Sp.list
cut -f2 downreg_Sp.txt | grep -v "x" > downreg_Sp.list

# Get the upregulated protein IDs
cat upreg_Sp.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Sp_protID.list
wc -l upreg_Sp_protID.list

# Get the downregulated protein IDs
cat downreg_Sp.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Sp_protID.list
wc -l downreg_Sp_protID.list
```


```r
upSp_tbl <- read.table(file = "upreg_Sp_protID.list")
downSp_tbl <- read.table(file = "downreg_Sp_protID.list")

upSp_IDs <- as.character(upSp_tbl$V1)
downSp_IDs <- as.character(downSp_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upSp_BP <- test_GO(upSp_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upSp_MF <- test_GO(upSp_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upSp_CC <- test_GO(upSp_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downSp_BP <- test_GO(downSp_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downSp_MF <- test_GO(downSp_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downSp_CC <- test_GO(downSp_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upSp_MF), 20)
```

```
##        GOMFID      Pvalue  OddsRatio    ExpCount Count Size
## 1  GO:0003777 0.000421195  25.816000 0.154128440     3   18
## 2  GO:0016917 0.008562691        Inf 0.008562691     1    1
## 3  GO:0004965 0.008562691        Inf 0.008562691     1    1
## 4  GO:0008798 0.008562691        Inf 0.008562691     1    1
## 5  GO:0004344 0.008562691        Inf 0.008562691     1    1
## 6  GO:0003937 0.008562691        Inf 0.008562691     1    1
## 7  GO:0004643 0.008562691        Inf 0.008562691     1    1
## 8  GO:0003774 0.015844431   6.364000 0.539449541     3   63
## 9  GO:0047770 0.017054660 120.037037 0.017125382     1    2
## 10 GO:0004605 0.017054660 120.037037 0.017125382     1    2
## 11 GO:0043022 0.017373832  11.258741 0.205504587     2   24
## 12 GO:0004095 0.025476468  60.000000 0.025688073     1    3
## 13 GO:0004054 0.025476468  60.000000 0.025688073     1    3
## 14 GO:0016416 0.025476468  60.000000 0.025688073     1    3
## 15 GO:0070567 0.025476468  60.000000 0.025688073     1    3
## 16 GO:0016775 0.025476468  60.000000 0.025688073     1    3
## 17 GO:0008066 0.025476468  60.000000 0.025688073     1    3
## 18 GO:0003743 0.031701527   7.967742 0.282568807     2   33
## 19 GO:0001094 0.033828674  39.987654 0.034250765     1    4
## 20 GO:0008113 0.033828674  39.987654 0.034250765     1    4
##                                                                  Term
## 1                                          microtubule motor activity
## 2                                              GABA receptor activity
## 3                            G-protein coupled GABA receptor activity
## 4                                    beta-aspartyl-peptidase activity
## 5                                      glucose dehydrogenase activity
## 6                                         IMP cyclohydrolase activity
## 7  phosphoribosylaminoimidazolecarboxamide formyltransferase activity
## 8                                                      motor activity
## 9                                      carboxylate reductase activity
## 10                        phosphatidate cytidylyltransferase activity
## 11                                                   ribosome binding
## 12                          carnitine O-palmitoyltransferase activity
## 13                                           arginine kinase activity
## 14                                    O-palmitoyltransferase activity
## 15                                      cytidylyltransferase activity
## 16         phosphotransferase activity, nitrogenous group as acceptor
## 17                                        glutamate receptor activity
## 18                             translation initiation factor activity
## 19                           TFIID-class transcription factor binding
## 20                  peptide-methionine (S)-S-oxide reductase activity
```

```r
head(summary(downSp_MF), 20)
```

```
##        GOMFID      Pvalue  OddsRatio    ExpCount Count Size
## 1  GO:0004648 0.003669725        Inf 0.003669725     1    1
## 2  GO:0019199 0.007327101 296.090909 0.007339450     1    2
## 3  GO:0015369 0.007327101 296.090909 0.007339450     1    2
## 4  GO:0005516 0.009798523  16.090000 0.154128440     2   42
## 5  GO:0051139 0.010972167 148.000000 0.011009174     1    3
## 6  GO:0005096 0.016457809  12.094340 0.201834862     2   55
## 7  GO:0015368 0.018225517  73.954545 0.018348624     1    5
## 8  GO:0005509 0.022586957  10.142857 0.238532110     2   65
## 9  GO:0015252 0.025430076  49.272727 0.025688073     1    7
## 10 GO:0016174 0.025430076  49.272727 0.025688073     1    7
## 11 GO:0016175 0.025430076  49.272727 0.025688073     1    7
## 12 GO:0030171 0.025430076  49.272727 0.025688073     1    7
## 13 GO:0019826 0.025430076  49.272727 0.025688073     1    7
## 14 GO:0001965 0.029014151  42.220779 0.029357798     1    8
## 15 GO:0050664 0.029014151  42.220779 0.029357798     1    8
## 16 GO:0015299 0.039694010  29.527273 0.040366972     1   11
## 17 GO:0019825 0.039694010  29.527273 0.040366972     1   11
## 18 GO:0030695 0.047370830   6.658947 0.355963303     2   97
## 19 GO:0030234 0.048610875   4.388406 0.855045872     3  233
##                                                              Term
## 1     O-phospho-L-serine:2-oxoglutarate aminotransferase activity
## 2                  transmembrane receptor protein kinase activity
## 3                              calcium:proton antiporter activity
## 4                                              calmodulin binding
## 5                            metal ion:proton antiporter activity
## 6                                       GTPase activator activity
## 7                              calcium:cation antiporter activity
## 8                                             calcium ion binding
## 9                                   hydrogen ion channel activity
## 10                                       NAD(P)H oxidase activity
## 11                   superoxide-generating NADPH oxidase activity
## 12                          voltage-gated proton channel activity
## 13                                         oxygen sensor activity
## 14                                G-protein alpha-subunit binding
## 15 oxidoreductase activity, acting on NAD(P)H, oxygen as acceptor
## 16                              solute:proton antiporter activity
## 17                                                 oxygen binding
## 18                                      GTPase regulator activity
## 19                                      enzyme regulator activity
```

As in Serum, seem like amino acid / peptide activity is up-regulated while some oxidation processes are down-regulated.

- **Top 50 highly expressed genes across samples**

From the results above regarding the top 50 highly expressed genes across samples, a difference between 'specific' media and 'rich' was shown. Thus, the annotation for those genes can be extracted from the catalogs. 


```r
# Get the top 50 genes from previously selections 
top_50_HEG <- rownames(assay(rld)[select_2, ])

# Write the genes on a file
write.table(as.character(top_50_HEG), file = "top50.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 top50.txt | grep -v "x" > top50.list

# Get the downregulated protein IDs
cat top50.list | while read line ; do egrep "gene_id "+$line Aplke1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > top50_protID.list
wc -l top50_protID.list

# Get the GO terms from the catalog
cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "biological_process" | cut -f3 | sort | uniq -c > top50_GO_BP.list

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "molecular_function" | cut -f3 | sort | uniq -c > top50_GO_MF.list

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "cellular_component" | cut -f3 | sort | uniq -c > top50_GO_CC.list

# Get the KEGG annotations from the catalog

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aplke1_GeneCatalog_proteins_20121220_KEGG.tab ; done | cut -f7 | egrep -v "\N" | sort | uniq -c > top50_KEGG.list

```


```r
# Writing the tables and data frames
Top50_GO_MF_table <- read.table(file="top50_GO_MF.list", sep = "\t", header=FALSE)
colnames(Top50_GO_MF_table) <- "Molecular Function"
Top50_GO_MF_df <- as.data.frame(Top50_GO_MF_table)

Top50_KEGG_table <- read.table(file="top50_KEGG.list", sep = "\t", header=FALSE)
colnames(Top50_KEGG_table) <- "KEGG pathways"
Top50_KEGG_df <- as.data.frame(Top50_KEGG_table)

# Making the tables
pandoc.table(Top50_GO_MF_df, justify = c("left"), split.table = Inf, caption = "Annotation of Molecular Function GO terms for TOP50 highly expressed genes", style = "rmarkdown")
```



| Molecular Function                                                          |
|:----------------------------------------------------------------------------|
| 1 1-aminocyclopropane-1-carboxylate synthase activity                       |
| 2 ATP binding                                                               |
| 2 ATPase activity                                                           |
| 1 CTD phosphatase activity                                                  |
| 6 DNA binding                                                               |
| 1 GTP binding                                                               |
| 1 acid phosphatase activity                                                 |
| 3 actin binding                                                             |
| 1 alanine transaminase activity                                             |
| 1 arginine kinase activity                                                  |
| 1 bile acid:sodium symporter activity                                       |
| 2 binding                                                                   |
| 2 calcium ion binding                                                       |
| 1 calcium-dependent protein serine/threonine phosphatase activity           |
| 1 calcium-dependent protein serine/threonine phosphatase regulator activity |
| 1 carboxypeptidase A activity                                               |
| 2 catalytic activity                                                        |
| 1 chromatin binding                                                         |
| 1 glutamate-ammonia ligase activity                                         |
| 1 kinase activity                                                           |
| 1 magnesium-dependent protein serine/threonine phosphatase activity         |
| 1 multiple inositol-polyphosphate phosphatase activity                      |
| 1 myosin phosphatase activity                                               |
| 1 myosin phosphatase regulator activity                                     |
| 2 nucleoside-triphosphatase activity                                        |
| 2 nucleotide binding                                                        |
| 1 ornithine decarboxylase activity                                          |
| 1 phosphoprotein phosphatase activity                                       |
| 1 phosphoric monoester hydrolase activity                                   |
| 1 protein serine/threonine phosphatase activity                             |
| 1 pyridoxal phosphate binding                                               |
| 1 structural constituent of ribosome                                        |
| 2 transcription factor activity                                             |
| 1 transferase activity, transferring nitrogenous groups                     |
| 1 transferase activity, transferring phosphorus-containing groups           |
| 1 transporter activity                                                      |
| 2 zinc ion binding                                                          |

Table: Annotation of Molecular Function GO terms for TOP50 highly expressed genes

```r
pandoc.table(Top50_KEGG_df, justify = c("left"), split.table = Inf, caption = "KEGG annotations for TOP50 highly expressed genes", style = "rmarkdown")
```



| KEGG pathways                               |
|:--------------------------------------------|
| 1 Alanine and aspartate metabolism          |
| 1 Alkaloid biosynthesis II                  |
| 2 Arginine and proline metabolism           |
| 1 Carbon fixation                           |
| 2 Glutamate metabolism                      |
| 1 Inositol phosphate metabolism             |
| 1 Peptidoglycan biosynthesis                |
| 1 Urea cycle and metabolism of amino groups |

Table: KEGG annotations for TOP50 highly expressed genes

