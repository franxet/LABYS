# Aurantiochytrium limacinum



## Building DESeq2 parameters




```r
# Set the directory were the HTSeq count files are located.
HTSeq <- "."

# Find the files in the directory for the counts using a common pattern. 
HTSeqFiles <- grep("counts", list.files(HTSeq), value = TRUE)
HTSeqFiles
```

```
## [1] "Aurli1.790ByPlus.nd.counts" "Aurli1.artemia.nd.counts"  
## [3] "Aurli1.serum.nd.counts"     "Aurli1.spartina.nd.counts"
```


```r
# Sample names.
sample <- c("Aurli1.790ByPlus", "Aurli1.artemia", "Aurli1.serum", "Aurli1.spartina")

# Build a sample table.
sampleTable <- data.frame(sampleName=sample, fileName=HTSeqFiles, sample=sample)
```


```r
# Building the DESeqDataSet whcih stores the count data for DESeq2.
dds <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable, directory = HTSeq, design = ~ sample)

# Number of genes.
nrow(dds)
```

```
## [1] 15529
```


```r
# Removing genes with no expressiong in all samples.
dds <- dds[ rowSums(counts(dds)) > 0, ]

# Number of genes after removing the zero expressed ones.
nrow(dds)
```

```
## [1] 14909
```

```r
# Apply DESeq for samples without replicates.
dds <- DESeq(dds)
```

```
## estimating size factors
```

```
## estimating dispersions
```

```
## Warning in checkForExperimentalReplicates(object, modelMatrix): same number of samples and coefficients to fit,
##   estimating dispersion by treating samples as replicates.
##   read the ?DESeq section on 'Experiments without replicates'
```

```
## gene-wise dispersion estimates
```

```
## mean-dispersion relationship
```

```
## final dispersion estimates
```

```
## fitting model and testing
```

```r
# Getting the raw counts (i.e. non-normalized counts)
raw_counts <- counts(dds, normalized = FALSE)
```



## Inspecting the samples

The results below show how Spartina and Serum are the two conditions with more mean genral expression. 


```r
## Distribution of gene counts with boxplots

# Log-transform to make numbers on scale (+1 to avoid zeroes)
pseudoCount <- log2(as.data.frame(raw_counts + 1)) 

# Melt the data frame
molten.pC <- melt(pseudoCount,
                 variable.name = "Sample",
                 value.name = "value")
```

```
## No id variables; using all as measure variables
```

```r
# Convert cellNr to factor
molten.pC$Sample <- as.factor(molten.pC$Sample) 

# Extracting the first character (substr), convert to factor with labels and levels specified.
molten.pC$Condition <- as.character(factor(substr(molten.pC$Sample, 11, 12), 
                                          levels = c("By", "em", "um", "rt"), labels = c("790ByPlus", "Artemia", "Serum", "Spartina")))

# Boxplot
box <- ggplot(data = molten.pC, aes(x = Sample, y = value, fill = Condition)) +
  geom_boxplot() +
  scale_y_continuous(expand = c(0, 0)) +
  ylab("log2(+1) count") +
  xlab("Sample") +
  theme(axis.text.x = element_text(size = 6),
        axis.text.y = element_text(size = 8),
        axis.title.y = element_text(size = 12),
        axis.title.x = element_text(size = 12))
box
```

<img src="aurli1_files/figure-html/unnamed-chunk-7-1.png" style="display: block; margin: auto;" />

```r
# Same information but in form of a density distribution

ggplot(molten.pC, aes(x = value, colour = Condition, fill = Condition)) + ylim(c(0, 0.2)) +
  geom_density(alpha = 0.2, size = 1.25) + facet_wrap(~ Condition) +
  theme(legend.position = "top") + xlab(expression(log[2](count + 1)))
```

<img src="aurli1_files/figure-html/unnamed-chunk-7-2.png" style="display: block; margin: auto;" />

## DESeq2 results

See below the code and output results for the comparison against 790ByPlus at the top 1000 genes. Code not shown for the other 3 conditions. 


```r
## Results DESeq2 comparing each condition against the others.

# Against 790ByPlus (Bp)
res_ArBp <- results(dds, contrast = c("sample","Aurli1.artemia","Aurli1.790ByPlus"))
res_SeBp <- results(dds, contrast = c("sample","Aurli1.serum","Aurli1.790ByPlus"))
res_SpBp <- results(dds, contrast = c("sample","Aurli1.spartina","Aurli1.790ByPlus"))

# Down-regulated in 790ByPlus
ArBp <- rownames(head(res_ArBp[ order(res_ArBp$log2FoldChange, decreasing = TRUE), ], 1000))
SeBp <- rownames(head(res_SeBp[ order(res_SeBp$log2FoldChange, decreasing = TRUE), ], 1000))
SpBp <- rownames(head(res_SpBp[ order(res_SpBp$log2FoldChange, decreasing = TRUE), ], 1000))

# Up-regulated in 790ByPlus
ArBp_2 <- rownames(head(res_ArBp[ order(res_ArBp$log2FoldChange), ], 1000))
SeBp_2 <- rownames(head(res_SeBp[ order(res_SeBp$log2FoldChange), ], 1000))
SpBp_2 <- rownames(head(res_SpBp[ order(res_SpBp$log2FoldChange), ], 1000))

# Drawing Venn-diagrams for both up and down regulated genes. 
vp <- venn.diagram(list(Ar=ArBp, Se=SeBp, Sp=SpBp), fill = 1:3, alpha=0.3, filename=NULL)
vp_2 <- venn.diagram(list(Ar=ArBp_2, Se=SeBp_2, Sp=SpBp_2), fill = 1:3, alpha=0.3, filename=NULL)

# Plotting them together
grid.arrange(gTree(children = vp), gTree(children = vp_2), ncol=2, left="# Up-regulated genes", right="# Down-regulated genes", top = "Top 1000 genes Up and Down regulated in 790ByPlus")
```

<img src="aurli1_files/figure-html/unnamed-chunk-8-1.png" style="display: block; margin: auto;" />

<img src="aurli1_files/figure-html/unnamed-chunk-9-1.png" style="display: block; margin: auto;" /><img src="aurli1_files/figure-html/unnamed-chunk-9-2.png" style="display: block; margin: auto;" /><img src="aurli1_files/figure-html/unnamed-chunk-9-3.png" style="display: block; margin: auto;" />


* **Regularized log transformation**

First, let's take a look at the genes who are highly expressed across samples. It seems like Aurli1 reacts differently to 790ByPlus than the rest of samples. 


```r
## Transformation

# Regularized log transformation - supposed to be the best for RNAseq data.
rld <- rlog(dds)

## Heatmaps
hmcol <- colorRampPalette(brewer.pal(9, "GnBu"))(100)
hmcol_2 <- colorRampPalette(brewer.pal(9, "YlOrRd"))(100)


# Select the 50 most highly expressed genes (highest mean values across rows) using normalized counts.
select_2 <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:50]
heatmap.2(assay(rld)[select_2, ], col = hmcol,
          Rowv = TRUE, Colv = TRUE, scale=c("row"),
          dendrogram="both", trace="none", margin=c(10, 6), cexRow = 0.7, cexCol = 0.9)
```

<img src="aurli1_files/figure-html/unnamed-chunk-10-1.png" style="display: block; margin: auto;" />

Now, the idea is to take those genes in the intersections from the Venn Diagrams and see their expression patterns in a heatmap. First, the top 50 more expressed genes within the genes of interest (the intersections) and latter the genes for each condition. 


```r
# Getting the union of the intersections
mygenes_1 <- Reduce(intersect, list(ArBp, SeBp, SpBp)) 
mygenes_2 <- Reduce(intersect, list(BpAr, SeAr, SpAr))
mygenes_3 <- Reduce(intersect, list(BpSe, ArSe, SpSe))
mygenes_4 <- Reduce(intersect, list(BpSp, ArSp, SeSp))
mygenes_5 <- Reduce(intersect, list(ArBp_2, SeBp_2, SpBp_2)) 
mygenes_6 <- Reduce(intersect, list(BpAr_2, SeAr_2, SpAr_2))
mygenes_7 <- Reduce(intersect, list(BpSe_2, ArSe_2, SpSe_2))
mygenes_8 <- Reduce(intersect, list(BpSp_2, ArSp_2, SeSp_2))

mygenes <- Reduce(union, list(mygenes_1, mygenes_2, mygenes_3, mygenes_4, mygenes_5, mygenes_6, mygenes_7, mygenes_8))

# Select the 50 most highly expressed genes (highest mean values across rows) of the union using normalized counts.
select <- order(rownames(dds) %in% mygenes, rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:50]
heatmap.2(assay(rld)[select, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aurli1_files/figure-html/unnamed-chunk-11-1.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of 790ByPlus
select_bp <- order(rownames(dds) %in% union(mygenes_1, mygenes_5), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_bp, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aurli1_files/figure-html/unnamed-chunk-11-2.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Artemia
select_ar <- order(rownames(dds) %in% union(mygenes_2, mygenes_6), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_ar, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aurli1_files/figure-html/unnamed-chunk-11-3.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Serum
select_se <- order(rownames(dds) %in% union(mygenes_3, mygenes_7), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_se, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aurli1_files/figure-html/unnamed-chunk-11-4.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Spartina
select_sp <- order(rownames(dds) %in% union(mygenes_4, mygenes_8), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_sp, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="aurli1_files/figure-html/unnamed-chunk-11-5.png" style="display: block; margin: auto;" />

As before, it seems that those genes that are up-regulated in 790ByPlus are down regulated in the other 3 conditions and the other way around.  

When clustering with `Hclust`, 790ByPlus is very diferent from the other mediums. This is very similar to what was shown in the heatmaps. 


```r
# Heatmap of sample-to-sample distances (uses all genes in the sample)
distsRL <- dist(t(assay(rld)))
mat <- as.matrix(distsRL)
heatmap.2(mat, trace="none", col = rev(hmcol_2), margin=c(13, 13), main = "Sample-to-sample distances (rlog)")
```

<img src="aurli1_files/figure-html/unnamed-chunk-12-1.png" style="display: block; margin: auto;" />

```r
# Using the 1000 most highly expressed genes (selected by mean expression across samples):
select <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:1000]
distsRL <- dist(t(assay(rld)[select,]))
hc = hclust(distsRL)
plot(hc, main="Sample-to-sample distances, 1000 top genes (rlog)")
```

<img src="aurli1_files/figure-html/unnamed-chunk-12-2.png" style="display: block; margin: auto;" />

```r
# Selecting genes based on variance (the varFilter in the genefilter package is especially designed for gene data)
m.var <- varFilter(assay(rld), var.func=IQR, var.cutoff=0.6, filterByQuantile=TRUE)
distsRL <- dist(t(m.var))
hc = hclust(distsRL)
plot(hc, main="Sample-to-sample distances, variable genes (rlog)")
```

<img src="aurli1_files/figure-html/unnamed-chunk-12-3.png" style="display: block; margin: auto;" />

# Functional Analyses

- Enrichment Analysis 


```bash

# First, parse the eggNOG-mapper annotations file to the tabular format GOstatsPlus needs
python ../convertGOstatsPlus.py Aurli1_GeneCatalog_proteins_20120618_one_line.fasta.emapper.annotations Aurli1_GeneCatalog_proteins_20120618.gostats

# Second, change the names to the protein IDs
awk 'BEGIN {OFS="\t"};  { split($1, a, "|"); print a[3], $2, $3 }' Aurli1_GeneCatalog_proteins_20120618.gostats > Aurli1_GeneCatalog_proteins_20120618.annot

```

After the annot file is done, the GOstatsPlus package can be used (GOstats, GOstatsPlus and GSEABase libraries are loaded at the beginning of the file).


```r
# Create the GOstats object and save it, in case that we want to re-run the sript. These are all the annotations available for Aurli1 proteins.
if(file.exists("Aurli1_gsc.rda")){
  load(file="Aurli1_gsc.rda")
}else{
  fn_annot = "Aurli1_GeneCatalog_proteins_20120618.annot"
  GO_gsc = b2g_to_gsc(file = fn_annot, organism = "Aurli1")
  save(GO_gsc, file = "Aurli1_gsc.rda")
}
```

- **Top expressed genes by condition**

The results below do not show any substantial up or down regulation pattern in any of the conditions. There are signals of biosynthesis of molecules, lipids and a lot of transporters.

* 790ByPlus 


```r
# Write genes on a file
write.table(as.character(mygenes_1), file = "upreg_790BP.txt", sep = "\t")
write.table(as.character(mygenes_5), file = "downreg_790BP.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_790BP.txt | grep -v "x" > upreg_790BP.list
cut -f2 downreg_790BP.txt | grep -v "x" > downreg_790BP.list

# Get the upregulated protein IDs
cat upreg_790BP.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_790BP_protID.list
wc -l upreg_790BP_protID.list

# Get the downregulated protein IDs
cat downreg_790BP.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_790BP_protID.list
wc -l downreg_790BP_protID.list
```


```r
upBP_tbl <- read.table(file = "upreg_790BP_protID.list")
downBP_tbl <- read.table(file = "downreg_790BP_protID.list")

upBP_IDs <- as.character(upBP_tbl$V1)
downBP_IDs <- as.character(downBP_tbl$V1)
```



```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upBP_BP <- test_GO(upBP_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upBP_MF <- test_GO(upBP_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upBP_CC <- test_GO(upBP_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downBP_BP <- test_GO(downBP_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downBP_MF <- test_GO(downBP_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downBP_CC <- test_GO(downBP_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upBP_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0010490 0.006653167       Inf 0.006653167     1    1
## 2  GO:0010489 0.006653167       Inf 0.006653167     1    1
## 3  GO:0016206 0.006653167       Inf 0.006653167     1    1
## 4  GO:0003827 0.006653167       Inf 0.006653167     1    1
## 5  GO:0008830 0.006653167       Inf 0.006653167     1    1
## 6  GO:0008831 0.006653167       Inf 0.006653167     1    1
## 7  GO:0009020 0.006653167       Inf 0.006653167     1    1
## 8  GO:0004820 0.006653167       Inf 0.006653167     1    1
## 9  GO:0004252 0.012490650  13.53175 0.172982355     2   26
## 10 GO:0015113 0.013263983 156.04545 0.013306335     1    2
## 11 GO:0016423 0.013263983 156.04545 0.013306335     1    2
## 12 GO:0015513 0.013263983 156.04545 0.013306335     1    2
## 13 GO:0004780 0.013263983 156.04545 0.013306335     1    2
## 14 GO:0022832 0.017514882  11.18227 0.206248192     2   31
## 15 GO:0015112 0.019832703  78.00000 0.019959502     1    3
## 16 GO:0015563 0.019832703  78.00000 0.019959502     1    3
## 17 GO:0045505 0.019832703  78.00000 0.019959502     1    3
## 18 GO:0004779 0.019832703  78.00000 0.019959502     1    3
## 19 GO:0008236 0.023257409   9.52381 0.239514030     2   36
## 20 GO:0017171 0.024487813   9.24898 0.246167197     2   37
##                                                                              Term
## 1                                   UDP-4-keto-rhamnose-4-keto-reductase activity
## 2                               UDP-4-keto-6-deoxy-glucose-3,5-epimerase activity
## 3                                           catechol O-methyltransferase activity
## 4  alpha-1,3-mannosylglycoprotein 2-beta-N-acetylglucosaminyltransferase activity
## 5                                   dTDP-4-dehydrorhamnose 3,5-epimerase activity
## 6                                       dTDP-4-dehydrorhamnose reductase activity
## 7                               tRNA (guanosine-2'-O-)-methyltransferase activity
## 8                                                    glycine-tRNA ligase activity
## 9                                              serine-type endopeptidase activity
## 10                                     nitrite transmembrane transporter activity
## 11                                      tRNA (guanine) methyltransferase activity
## 12                              nitrite uptake transmembrane transporter activity
## 13                                     sulfate adenylyltransferase (ADP) activity
## 14                                                 voltage-gated channel activity
## 15                                     nitrate transmembrane transporter activity
## 16                                      uptake transmembrane transporter activity
## 17                                              dynein intermediate chain binding
## 18                                           sulfate adenylyltransferase activity
## 19                                                 serine-type peptidase activity
## 20                                                      serine hydrolase activity
```

```r
head(summary(downBP_MF), 20)
```

```
##        GOMFID       Pvalue  OddsRatio   ExpCount Count Size
## 1  GO:0015291 4.675314e-11  42.337500 0.39051200     9   54
## 2  GO:0015114 1.059522e-08 653.523810 0.03615852     4    5
## 3  GO:0005436 1.059522e-08 653.523810 0.03615852     4    5
## 4  GO:0015321 1.059522e-08 653.523810 0.03615852     4    5
## 5  GO:1901677 2.546907e-08  48.947368 0.20248771     6   28
## 6  GO:0008509 5.019169e-08  21.653505 0.58576801     8   81
## 7  GO:0015293 7.561245e-08  71.250000 0.12293896     5   17
## 8  GO:0015294 7.561245e-08  71.250000 0.12293896     5   17
## 9  GO:0022804 8.623390e-08  16.371711 0.88949957     9  123
## 10 GO:0015103 2.438744e-07  53.375000 0.15186578     5   21
## 11 GO:0031402 2.618408e-07 130.552381 0.06508533     4    9
## 12 GO:0042301 4.342777e-07 108.761905 0.07231704     4   10
## 13 GO:0015081 6.245099e-07  42.650000 0.18079259     5   25
## 14 GO:0015296 1.013716e-06  81.523810 0.08678045     4   12
## 15 GO:0031420 1.013716e-06  81.523810 0.08678045     4   12
## 16 GO:0022892 1.161226e-06   9.351772 2.00318195    11  277
## 17 GO:0022891 3.501317e-06   9.028249 1.77899913    10  246
## 18 GO:0015075 7.640224e-06   9.138505 1.50419439     9  208
## 19 GO:0022857 1.028438e-05   7.902622 2.00318195    10  277
## 20 GO:0005215 1.183985e-05   7.168774 2.53109633    11  350
##                                                             Term
## 1            secondary active transmembrane transporter activity
## 2               phosphate ion transmembrane transporter activity
## 3                            sodium:phosphate symporter activity
## 4  sodium-dependent phosphate transmembrane transporter activity
## 5                   phosphate transmembrane transporter activity
## 6                       anion transmembrane transporter activity
## 7                                             symporter activity
## 8                               solute:cation symporter activity
## 9                      active transmembrane transporter activity
## 10            inorganic anion transmembrane transporter activity
## 11                                            sodium ion binding
## 12                                         phosphate ion binding
## 13                 sodium ion transmembrane transporter activity
## 14                               anion:cation symporter activity
## 15                                      alkali metal ion binding
## 16                       substrate-specific transporter activity
## 17         substrate-specific transmembrane transporter activity
## 18                        ion transmembrane transporter activity
## 19                            transmembrane transporter activity
## 20                                          transporter activity
```

* Artemia


```r
# Write genes on a file
write.table(as.character(mygenes_2), file = "upreg_Ar.txt", sep = "\t")
write.table(as.character(mygenes_6), file = "downreg_Ar.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Ar.txt | grep -v "x" > upreg_Ar.list
cut -f2 downreg_Ar.txt | grep -v "x" > downreg_Ar.list

# Get the upregulated protein IDs
cat upreg_Ar.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Ar_protID.list
wc -l upreg_Ar_protID.list

# Get the downregulated protein IDs
cat downreg_Ar.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Ar_protID.list
wc -l downreg_Ar_protID.list
```


```r
upAr_tbl <- read.table(file = "upreg_Ar_protID.list")
downAr_tbl <- read.table(file = "downreg_Ar_protID.list")

upAr_IDs <- as.character(upAr_tbl$V1)
downAr_IDs <- as.character(downAr_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upAr_BP <- test_GO(upAr_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upAr_MF <- test_GO(upAr_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upAr_CC <- test_GO(upAr_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downAr_BP <- test_GO(downAr_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downAr_MF <- test_GO(downAr_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downAr_CC <- test_GO(downAr_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upAr_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio   ExpCount Count Size
## 1  GO:0004383 1.827888e-05 93.463636 0.05785363     3    8
## 2  GO:0004016 1.759242e-04 35.863636 0.11570726     3   16
## 3  GO:0016849 1.759242e-04 35.863636 0.11570726     3   16
## 4  GO:0009975 3.000755e-04 29.113636 0.13740237     3   19
## 5  GO:0030145 1.078747e-03 17.863636 0.20971941     3   29
## 6  GO:0005516 3.407846e-03 11.563636 0.31096326     3   43
## 7  GO:0046872 3.641056e-03  3.930117 2.71188892     8  375
## 8  GO:0043169 4.508548e-03  3.779567 2.80590107     8  388
## 9  GO:0005509 1.051094e-02  5.349475 0.88226786     4  122
## 10 GO:0004672 1.319000e-02  4.105330 1.46080417     5  202
## 11 GO:0031433 2.154479e-02 71.458333 0.02169511     1    3
## 12 GO:0005251 2.154479e-02 71.458333 0.02169511     1    3
## 13 GO:0004674 2.560680e-02  4.027035 1.14984090     4  159
## 14 GO:0030172 2.862683e-02 47.625000 0.02892682     1    4
## 15 GO:0005523 2.862683e-02 47.625000 0.02892682     1    4
## 16 GO:0043167 3.134868e-02  2.392157 5.48163147    10  758
## 17 GO:0005200 3.159930e-02  7.978848 0.28203645     2   39
## 18 GO:0016773 3.193727e-02  3.209677 1.82962106     5  253
## 19 GO:0015114 3.565965e-02 35.708333 0.03615852     1    5
## 20 GO:0005436 3.565965e-02 35.708333 0.03615852     1    5
##                                                      Term
## 1                              guanylate cyclase activity
## 2                              adenylate cyclase activity
## 3                        phosphorus-oxygen lyase activity
## 4                                        cyclase activity
## 5                                   manganese ion binding
## 6                                      calmodulin binding
## 7                                       metal ion binding
## 8                                          cation binding
## 9                                     calcium ion binding
## 10                                protein kinase activity
## 11                                     telethonin binding
## 12           delayed rectifier potassium channel activity
## 13               protein serine/threonine kinase activity
## 14                                     troponin C binding
## 15                                    tropomyosin binding
## 16                                            ion binding
## 17                 structural constituent of cytoskeleton
## 18 phosphotransferase activity, alcohol group as acceptor
## 19       phosphate ion transmembrane transporter activity
## 20                    sodium:phosphate symporter activity
```

```r
head(summary(downAr_MF), 20)
```

```
##       GOMFID     Pvalue OddsRatio   ExpCount Count Size
## 1 GO:0008237 0.02129435  94.97222 0.02140584     1   37
##                        Term
## 1 metallopeptidase activity
```

* Serum 


```r
# Write genes on a file
write.table(as.character(mygenes_3), file = "upreg_Se.txt", sep = "\t")
write.table(as.character(mygenes_7), file = "downreg_Se.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Se.txt | grep -v "x" > upreg_Se.list
cut -f2 downreg_Se.txt | grep -v "x" > downreg_Se.list

# Get the upregulated protein IDs
cat upreg_Se.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Se_protID.list
wc -l upreg_Se_protID.list

# Get the downregulated protein IDs
cat downreg_Se.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Se_protID.list
wc -l downreg_Se_protID.list
```


```r
upSe_tbl <- read.table(file = "upreg_Se_protID.list")
downSe_tbl <- read.table(file = "downreg_Se_protID.list")

upSe_IDs <- as.character(upSe_tbl$V1)
downSe_IDs <- as.character(downSe_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upSe_BP <- test_GO(upSe_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upSe_MF <- test_GO(upSe_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upSe_CC <- test_GO(upSe_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downSe_BP <- test_GO(downSe_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downSe_MF <- test_GO(downSe_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downSe_CC <- test_GO(downSe_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```

```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upSe_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0050692 0.002314145       Inf 0.002314145     1    1
## 2  GO:0050694 0.002314145       Inf 0.002314145     1    1
## 3  GO:0050698 0.002314145       Inf 0.002314145     1    1
## 4  GO:0015254 0.004623603 492.57143 0.004628290     1    2
## 5  GO:0035259 0.004623603 492.57143 0.004628290     1    2
## 6  GO:0050656 0.004623603 492.57143 0.004628290     1    2
## 7  GO:0004851 0.004623603 492.57143 0.004628290     1    2
## 8  GO:0015168 0.006928382 246.21429 0.006942436     1    3
## 9  GO:0004499 0.006928382 246.21429 0.006942436     1    3
## 10 GO:0047372 0.006928382 246.21429 0.006942436     1    3
## 11 GO:0042134 0.006928382 246.21429 0.006942436     1    3
## 12 GO:0015166 0.009228490 164.09524 0.009256581     1    4
## 13 GO:0008169 0.009228490 164.09524 0.009256581     1    4
## 14 GO:0046966 0.011523935 123.03571 0.011570726     1    5
## 15 GO:0008146 0.013814726  98.40000 0.013884871     1    6
## 16 GO:0015665 0.013814726  98.40000 0.013884871     1    6
## 17 GO:0030331 0.013814726  98.40000 0.013884871     1    6
## 18 GO:0030374 0.016100869  81.97619 0.016199016     1    7
## 19 GO:0004806 0.016100869  81.97619 0.016199016     1    7
## 20 GO:0050681 0.020659249  61.44643 0.020827307     1    9
##                                                                    Term
## 1                                                    DBD domain binding
## 2                               galactose 3-O-sulfotransferase activity
## 3                                proteoglycan sulfotransferase activity
## 4                                             glycerol channel activity
## 5                                       glucocorticoid receptor binding
## 6                         3'-phosphoadenosine 5'-phosphosulfate binding
## 7                         uroporphyrin-III C-methyltransferase activity
## 8                           glycerol transmembrane transporter activity
## 9                            N,N-dimethylaniline monooxygenase activity
## 10                                         acylglycerol lipase activity
## 11                                      rRNA primary transcript binding
## 12                            polyol transmembrane transporter activity
## 13                                         C-methyltransferase activity
## 14                                     thyroid hormone receptor binding
## 15                                            sulfotransferase activity
## 16                           alcohol transmembrane transporter activity
## 17                                            estrogen receptor binding
## 18 ligand-dependent nuclear receptor transcription coactivator activity
## 19                                         triglyceride lipase activity
## 20                                            androgen receptor binding
```

```r
head(summary(downSe_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0004392 0.001446341       Inf 0.001446341     1    1
## 2  GO:0030600 0.002891008 862.75000 0.002892682     1    2
## 3  GO:0004630 0.002891008 862.75000 0.002892682     1    2
## 4  GO:0003984 0.004334002 431.25000 0.004339022     1    3
## 5  GO:0016788 0.005041399  16.66842 0.416546138     3  288
## 6  GO:0016723 0.007214978 215.50000 0.007231704     1    5
## 7  GO:0016744 0.007214978 215.50000 0.007231704     1    5
## 8  GO:0000293 0.007214978 215.50000 0.007231704     1    5
## 9  GO:0016722 0.010089282 143.58333 0.010124385     1    7
## 10 GO:0020037 0.015817918  86.05000 0.015909748     1   11
## 11 GO:0046906 0.015817918  86.05000 0.015909748     1   11
## 12 GO:0004620 0.022941384  57.28333 0.023141452     1   16
## 13 GO:0016298 0.039869512  31.71296 0.040497541     1   28
## 14 GO:0008081 0.041269527  30.57143 0.041943882     1   29
## 15 GO:0052689 0.045459777  27.58871 0.046282904     1   32
##                                                                      Term
## 1                                   heme oxygenase (decyclizing) activity
## 2                                              feruloyl esterase activity
## 3                                                phospholipase D activity
## 4                                          acetolactate synthase activity
## 5                               hydrolase activity, acting on ester bonds
## 6  oxidoreductase activity, oxidizing metal ions, NAD or NADP as acceptor
## 7           transferase activity, transferring aldehyde or ketonic groups
## 8                                       ferric-chelate reductase activity
## 9                           oxidoreductase activity, oxidizing metal ions
## 10                                                           heme binding
## 11                                                   tetrapyrrole binding
## 12                                                 phospholipase activity
## 13                                                        lipase activity
## 14                                  phosphoric diester hydrolase activity
## 15                                    carboxylic ester hydrolase activity
```

* Spartina


```r
# Write genes on a file
write.table(as.character(mygenes_4), file = "upreg_Sp.txt", sep = "\t")
write.table(as.character(mygenes_8), file = "downreg_Sp.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Sp.txt | grep -v "x" > upreg_Sp.list
cut -f2 downreg_Sp.txt | grep -v "x" > downreg_Sp.list

# Get the upregulated protein IDs
cat upreg_Sp.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Sp_protID.list
wc -l upreg_Sp_protID.list

# Get the downregulated protein IDs
cat downreg_Sp.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Sp_protID.list
wc -l downreg_Sp_protID.list
```


```r
upSp_tbl <- read.table(file = "upreg_Sp_protID.list")
downSp_tbl <- read.table(file = "downreg_Sp_protID.list")

upSp_IDs <- as.character(upSp_tbl$V1)
downSp_IDs <- as.character(downSp_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upSp_BP <- test_GO(upSp_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upSp_MF <- test_GO(upSp_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upSp_CC <- test_GO(upSp_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downSp_BP <- test_GO(downSp_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downSp_MF <- test_GO(downSp_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downSp_CC <- test_GO(downSp_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upSp_MF), 20)
```

```
##        GOMFID       Pvalue  OddsRatio    ExpCount Count Size
## 1  GO:0015114 0.0003147592 127.185185 0.028926815     2    5
## 2  GO:0005436 0.0003147592 127.185185 0.028926815     2    5
## 3  GO:0015321 0.0003147592 127.185185 0.028926815     2    5
## 4  GO:0031402 0.0011174878  54.444444 0.052068267     2    9
## 5  GO:0042301 0.0013920156  47.625000 0.057853630     2   10
## 6  GO:0004674 0.0017076483   7.106061 0.919872722     5  159
## 7  GO:0005516 0.0017638702  14.986765 0.248770610     3   43
## 8  GO:0015296 0.0020274920  38.077778 0.069424356     2   12
## 9  GO:0031420 0.0020274920  38.077778 0.069424356     2   12
## 10 GO:0015293 0.0041060024  25.348148 0.098351172     2   17
## 11 GO:0015294 0.0041060024  25.348148 0.098351172     2   17
## 12 GO:0004672 0.0048835153   5.482234 1.168643332     5  202
## 13 GO:0035727 0.0057853630        Inf 0.005785363     1    1
## 14 GO:0050565 0.0057853630        Inf 0.005785363     1    1
## 15 GO:0015103 0.0062528477  19.988304 0.121492624     2   21
## 16 GO:0015081 0.0088097517  16.492754 0.144634076     2   25
## 17 GO:0003779 0.0092215875   8.019873 0.445472953     3   77
## 18 GO:0004252 0.0095109220  15.800926 0.150419439     2   26
## 19 GO:1901677 0.0109856513  14.576923 0.161990165     2   28
## 20 GO:0070736 0.0115389200 180.842105 0.011570726     1    2
##                                                             Term
## 1               phosphate ion transmembrane transporter activity
## 2                            sodium:phosphate symporter activity
## 3  sodium-dependent phosphate transmembrane transporter activity
## 4                                             sodium ion binding
## 5                                          phosphate ion binding
## 6                       protein serine/threonine kinase activity
## 7                                             calmodulin binding
## 8                                anion:cation symporter activity
## 9                                       alkali metal ion binding
## 10                                            symporter activity
## 11                              solute:cation symporter activity
## 12                                       protein kinase activity
## 13                                 lysophosphatidic acid binding
## 14                                  aerobactin synthase activity
## 15            inorganic anion transmembrane transporter activity
## 16                 sodium ion transmembrane transporter activity
## 17                                                 actin binding
## 18                            serine-type endopeptidase activity
## 19                  phosphate transmembrane transporter activity
## 20                   protein-glycine ligase activity, initiating
```

```r
head(summary(downSp_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0005215 0.001029797       Inf 0.303731559     3  350
## 2  GO:0042895 0.005199297 344.90000 0.005206827     1    6
## 3  GO:0019534 0.005199297 344.90000 0.005206827     1    6
## 4  GO:0008493 0.005199297 344.90000 0.005206827     1    6
## 5  GO:0051119 0.006928382 246.21429 0.006942436     1    8
## 6  GO:0015250 0.006928382 246.21429 0.006942436     1    8
## 7  GO:0005372 0.006928382 246.21429 0.006942436     1    8
## 8  GO:0015144 0.010380540 156.50000 0.010413653     1   12
## 9  GO:1901476 0.010380540 156.50000 0.010413653     1   12
## 10 GO:0022891 0.014421322  26.31148 0.213479896     2  246
## 11 GO:0090484 0.015543762 101.08824 0.015620480     1   18
## 12 GO:0022892 0.018178498  23.12000 0.240381834     2  277
## 13 GO:0022857 0.018178498  23.12000 0.240381834     2  277
##                                                     Term
## 1                                   transporter activity
## 2                        antibiotic transporter activity
## 3                             toxin transporter activity
## 4                      tetracycline transporter activity
## 5               sugar transmembrane transporter activity
## 6                                 water channel activity
## 7               water transmembrane transporter activity
## 8        carbohydrate transmembrane transporter activity
## 9                      carbohydrate transporter activity
## 10 substrate-specific transmembrane transporter activity
## 11                             drug transporter activity
## 12               substrate-specific transporter activity
## 13                    transmembrane transporter activity
```

- **Top 50 highly expressed genes across samples**

From the results above regarding the top 50 highly expressed genes across samples. Thus, the annotation for those genes can be extracted from the catalogs. 


```r
# Get the top 50 genes from previously selections 
top_50_HEG <- rownames(assay(rld)[select_2, ])

# Write the genes on a file
write.table(as.character(top_50_HEG), file = "top50.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 top50.txt | grep -v "x" > top50.list

# Get the downregulated protein IDs
cat top50.list | while read line ; do egrep "gene_id "+$line Aurli1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > top50_protID.list
wc -l top50_protID.list

# Get the GO terms from the catalog
cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aurli1_GeneCatalog_proteins_20120618_GO.tab ; done | egrep "biological_process" | cut -f3 | sort | uniq -c > top50_GO_BP.list

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aurli1_GeneCatalog_proteins_20120618_GO.tab ; done | egrep "molecular_function" | cut -f3 | sort | uniq -c > top50_GO_MF.list

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aurli1_GeneCatalog_proteins_20120618_GO.tab ; done | egrep "cellular_component" | cut -f3 | sort | uniq -c > top50_GO_CC.list

# Get the KEGG annotations from the catalog

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Aurli1_GeneCatalog_proteins_20120618_KEGG.tab ; done | cut -f7 | egrep -v "\N" | sort | uniq -c > top50_KEGG.list

```


```r
# Writing the tables and data frames
Top50_GO_MF_table <- read.table(file="top50_GO_MF.list", sep = "\t", header=FALSE)
colnames(Top50_GO_MF_table) <- "Molecular Function"
Top50_GO_MF_df <- as.data.frame(Top50_GO_MF_table)

Top50_KEGG_table <- read.table(file="top50_KEGG.list", sep = "\t", header=FALSE)
colnames(Top50_KEGG_table) <- "KEGG pathways"
Top50_KEGG_df <- as.data.frame(Top50_KEGG_table)

# Making the tables
pandoc.table(Top50_GO_MF_df, justify = c("left"), split.table = Inf, caption = "Annotation of Molecular Function GO terms for TOP50 highly expressed genes", style = "rmarkdown")
```



| Molecular Function                                                                               |
|:-------------------------------------------------------------------------------------------------|
| 1 (R)-2-hydroxyglutarate dehydrogenase activity                                                  |
| 1 (R)-2-hydroxyisocaproate dehydrogenase activity                                                |
| 1 2-hydroxytetrahydrofuran dehydrogenase activity                                                |
| 1 3-keto sterol reductase activity                                                               |
| 1 3-ketoglucose-reductase activity                                                               |
| 1 5-exo-hydroxycamphor dehydrogenase activity                                                    |
| 5 ATP binding                                                                                    |
| 3 ATPase activity                                                                                |
| 1 ATPase activity, coupled to transmembrane movement of ions, phosphorylative mechanism          |
| 1 C-3 sterol dehydrogenase (C-4 sterol decarboxylase) activity                                   |
| 1 DNA binding                                                                                    |
| 1 GTP binding                                                                                    |
| 1 GTPase activity                                                                                |
| 1 RNA binding                                                                                    |
| 1 aldo-keto reductase activity                                                                   |
| 8 binding                                                                                        |
| 3 catalytic activity                                                                             |
| 1 chaperonin ATPase activity                                                                     |
| 2 chromatin binding                                                                              |
| 1 coenzyme binding                                                                               |
| 1 cysteine protease inhibitor activity                                                           |
| 1 epoxide dehydrogenase activity                                                                 |
| 1 gluconate dehydrogenase activity                                                               |
| 1 histone acetyltransferase activity                                                             |
| 1 hydrogen ion transporting ATP synthase activity, rotational mechanism                          |
| 1 hydrogen ion transporting ATPase activity, rotational mechanism                                |
| 1 hydrogen-exporting ATPase activity, phosphorylative mechanism                                  |
| 1 hydrogen-transporting two-sector ATPase activity                                               |
| 1 hydrolase activity, acting on acid anhydrides, catalyzing transmembrane movement of substances |
| 1 inositol oxygenase activity                                                                    |
| 1 isocitrate dehydrogenase activity                                                              |
| 1 mevaldate reductase activity                                                                   |
| 1 neurotransmitter:sodium symporter activity                                                     |
| 2 nucleoside-triphosphatase activity                                                             |
| 2 nucleotide binding                                                                             |
| 1 oxidoreductase activity                                                                        |
| 1 phenylcoumaran benzylic ether reductase activity                                               |
| 1 protein-synthesizing GTPase activity                                                           |
| 1 sodium:hydrogen antiporter activity                                                            |
| 1 steroid dehydrogenase activity                                                                 |
| 1 steroid dehydrogenase activity, acting on the CH-CH group of donors                            |
| 1 steroid dehydrogenase activity, acting on the CH-OH group of donors, NAD or NADP as acceptor   |
| 1 structural constituent of ribosome                                                             |
| 1 sugar:hydrogen symporter activity                                                              |
| 1 transcription factor activity                                                                  |
| 1 translation elongation factor activity                                                         |
| 1 translation initiation factor activity                                                         |
| 7 transporter activity                                                                           |
| 1 unfolded protein binding                                                                       |
| 1 zinc ion binding                                                                               |

Table: Annotation of Molecular Function GO terms for TOP50 highly expressed genes

```r
pandoc.table(Top50_KEGG_df, justify = c("left"), split.table = Inf, caption = "KEGG annotations for TOP50 highly expressed genes", style = "rmarkdown")
```



| KEGG pathways                              |
|:-------------------------------------------|
| 1   Flagellar assembly                     |
| 1   Type III secretion system              |
| 1 ATP synthesis                            |
| 1 Ascorbate and aldarate metabolism        |
| 1 Benzoate degradation via CoA ligation    |
| 1 Bile acid biosynthesis                   |
| 1 Butanoate metabolism                     |
| 1 Fructose and mannose metabolism          |
| 1 Galactose metabolism                     |
| 1 Glycerolipid metabolism                  |
| 1 Glycine, serine and threonine metabolism |
| 1 Inositol phosphate metabolism            |
| 1 Lysine degradation                       |
| 1 Oxidative phosphorylation                |
| 1 Tetrachloroethene degradation            |
| 1 Vitamin B6 metabolism                    |

Table: KEGG annotations for TOP50 highly expressed genes

