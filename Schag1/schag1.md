# Schizochytrium aggregatum



## Building DESeq2 parameters




```r
# Set the directory were the HTSeq count files are located.
HTSeq <- "."

# Find the files in the directory for the counts using a common pattern. 
HTSeqFiles <- grep("counts", list.files(HTSeq), value = TRUE)
HTSeqFiles
```

```
## [1] "Schag1.790ByPlus.nd.counts" "Schag1.artemia.nd.counts"  
## [3] "Schag1.serum.nd.counts"     "Schag1.spartina.nd.counts"
```


```r
# Sample names.
sample <- c("Schag1.790ByPlus", "Schag1.artemia", "Schag1.serum", "Schag1.spartina")

# Build a sample table.
sampleTable <- data.frame(sampleName=sample, fileName=HTSeqFiles, sample=sample)
```


```r
# Building the DESeqDataSet whcih stores the count data for DESeq2.
dds <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable, directory = HTSeq, design = ~ sample)

# Number of genes.
nrow(dds)
```

```
## [1] 11421
```


```r
# Removing genes with no expressiong in all samples.
dds <- dds[ rowSums(counts(dds)) > 0, ]

# Number of genes after removing the zero expressed ones.
nrow(dds)
```

```
## [1] 10998
```

```r
# Apply DESeq for samples without replicates.
dds <- DESeq(dds)
```

```
## estimating size factors
```

```
## estimating dispersions
```

```
## Warning in checkForExperimentalReplicates(object, modelMatrix): same number of samples and coefficients to fit,
##   estimating dispersion by treating samples as replicates.
##   read the ?DESeq section on 'Experiments without replicates'
```

```
## gene-wise dispersion estimates
```

```
## mean-dispersion relationship
```

```
## final dispersion estimates
```

```
## fitting model and testing
```

```r
# Getting the raw counts (i.e. non-normalized counts)
raw_counts <- counts(dds, normalized = FALSE)
```



## Inspecting the samples

The results below show how artemia is the condition with more mean genral expression, while 790ByPlus has the lowest.


```r
## Distribution of gene counts with boxplots

# Log-transform to make numbers on scale (+1 to avoid zeroes)
pseudoCount <- log2(as.data.frame(raw_counts + 1)) 

# Melt the data frame
molten.pC <- melt(pseudoCount,
                 variable.name = "Sample",
                 value.name = "value")
```

```
## No id variables; using all as measure variables
```

```r
# Convert cellNr to factor
molten.pC$Sample <- as.factor(molten.pC$Sample) 

# Extracting the first character (substr), convert to factor with labels and levels specified.
molten.pC$Condition <- as.character(factor(substr(molten.pC$Sample, 11, 12), 
                                          levels = c("By", "em", "um", "rt"), labels = c("790ByPlus", "Artemia", "Serum", "Spartina")))

# Boxplot
box <- ggplot(data = molten.pC, aes(x = Sample, y = value, fill = Condition)) +
  geom_boxplot() +
  scale_y_continuous(expand = c(0, 0)) +
  ylab("log2(+1) count") +
  xlab("Sample") +
  theme(axis.text.x = element_text(size = 6),
        axis.text.y = element_text(size = 8),
        axis.title.y = element_text(size = 12),
        axis.title.x = element_text(size = 12))
box
```

<img src="schag1_files/figure-html/unnamed-chunk-7-1.png" style="display: block; margin: auto;" />

```r
# Same information but in form of a density distribution

ggplot(molten.pC, aes(x = value, colour = Condition, fill = Condition)) + ylim(c(0, 0.2)) +
  geom_density(alpha = 0.2, size = 1.25) + facet_wrap(~ Condition) +
  theme(legend.position = "top") + xlab(expression(log[2](count + 1)))
```

<img src="schag1_files/figure-html/unnamed-chunk-7-2.png" style="display: block; margin: auto;" />

## DESeq2 results

See below the code and output results for the comparison against 790ByPlus at the top 1000 genes. Code not shown for the other 3 conditions. 


```r
## Results DESeq2 comparing each condition against the others.

# Against 790ByPlus (Bp)
res_ArBp <- results(dds, contrast = c("sample","Schag1.artemia","Schag1.790ByPlus"))
res_SeBp <- results(dds, contrast = c("sample","Schag1.serum","Schag1.790ByPlus"))
res_SpBp <- results(dds, contrast = c("sample","Schag1.spartina","Schag1.790ByPlus"))

# Down-regulated in 790ByPlus
ArBp <- rownames(head(res_ArBp[ order(res_ArBp$log2FoldChange, decreasing = TRUE), ], 1000))
SeBp <- rownames(head(res_SeBp[ order(res_SeBp$log2FoldChange, decreasing = TRUE), ], 1000))
SpBp <- rownames(head(res_SpBp[ order(res_SpBp$log2FoldChange, decreasing = TRUE), ], 1000))

# Up-regulated in 790ByPlus
ArBp_2 <- rownames(head(res_ArBp[ order(res_ArBp$log2FoldChange), ], 1000))
SeBp_2 <- rownames(head(res_SeBp[ order(res_SeBp$log2FoldChange), ], 1000))
SpBp_2 <- rownames(head(res_SpBp[ order(res_SpBp$log2FoldChange), ], 1000))

# Drawing Venn-diagrams for both up and down regulated genes. 
vp <- venn.diagram(list(Ar=ArBp, Se=SeBp, Sp=SpBp), fill = 1:3, alpha=0.3, filename=NULL)
vp_2 <- venn.diagram(list(Ar=ArBp_2, Se=SeBp_2, Sp=SpBp_2), fill = 1:3, alpha=0.3, filename=NULL)

# Plotting them together
grid.arrange(gTree(children = vp), gTree(children = vp_2), ncol=2, left="# Up-regulated genes", right="# Down-regulated genes", top = "Top 1000 genes Up and Down regulated in 790ByPlus")
```

<img src="schag1_files/figure-html/unnamed-chunk-8-1.png" style="display: block; margin: auto;" />

<img src="schag1_files/figure-html/unnamed-chunk-9-1.png" style="display: block; margin: auto;" /><img src="schag1_files/figure-html/unnamed-chunk-9-2.png" style="display: block; margin: auto;" /><img src="schag1_files/figure-html/unnamed-chunk-9-3.png" style="display: block; margin: auto;" />


* **Regularized log transformation**

First, let's take a look at the genes who are highly expressed across samples. It does not show a clear pattern of expression, but it appears as serum is very different from 790ByPlus and Artemia and more similar to Spartina. 


```r
## Transformation

# Regularized log transformation - supposed to be the best for RNAseq data.
rld <- rlog(dds)

## Heatmaps
hmcol <- colorRampPalette(brewer.pal(9, "GnBu"))(100)
hmcol_2 <- colorRampPalette(brewer.pal(9, "YlOrRd"))(100)


# Select the 50 most highly expressed genes (highest mean values across rows) using normalized counts.
select_2 <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:50]
heatmap.2(assay(rld)[select_2, ], col = hmcol,
          Rowv = TRUE, Colv = TRUE, scale=c("row"),
          dendrogram="both", trace="none", margin=c(10, 6), cexRow = 0.7, cexCol = 0.9)
```

<img src="schag1_files/figure-html/unnamed-chunk-10-1.png" style="display: block; margin: auto;" />

Now, the idea is to take those genes in the intersections from the Venn Diagrams and see their expression patterns in a heatmap. First, the top 50 more expressed genes within the genes of interest (the intersections) and latter the genes for each condition. 


```r
# Getting the union of the intersections
mygenes_1 <- Reduce(intersect, list(ArBp, SeBp, SpBp)) 
mygenes_2 <- Reduce(intersect, list(BpAr, SeAr, SpAr))
mygenes_3 <- Reduce(intersect, list(BpSe, ArSe, SpSe))
mygenes_4 <- Reduce(intersect, list(BpSp, ArSp, SeSp))
mygenes_5 <- Reduce(intersect, list(ArBp_2, SeBp_2, SpBp_2)) 
mygenes_6 <- Reduce(intersect, list(BpAr_2, SeAr_2, SpAr_2))
mygenes_7 <- Reduce(intersect, list(BpSe_2, ArSe_2, SpSe_2))
mygenes_8 <- Reduce(intersect, list(BpSp_2, ArSp_2, SeSp_2))

mygenes <- Reduce(union, list(mygenes_1, mygenes_2, mygenes_3, mygenes_4, mygenes_5, mygenes_6, mygenes_7, mygenes_8))

# Select the 50 most highly expressed genes (highest mean values across rows) of the union using normalized counts.
select <- order(rownames(dds) %in% mygenes, rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:50]
heatmap.2(assay(rld)[select, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="schag1_files/figure-html/unnamed-chunk-11-1.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of 790ByPlus
select_bp <- order(rownames(dds) %in% union(mygenes_1, mygenes_5), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_bp, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="schag1_files/figure-html/unnamed-chunk-11-2.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Artemia
select_ar <- order(rownames(dds) %in% union(mygenes_2, mygenes_6), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_ar, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="schag1_files/figure-html/unnamed-chunk-11-3.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Serum
select_se <- order(rownames(dds) %in% union(mygenes_3, mygenes_7), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_se, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="schag1_files/figure-html/unnamed-chunk-11-4.png" style="display: block; margin: auto;" />

```r
# Select the 100 more and less expressed genes of Spartina
select_sp <- order(rownames(dds) %in% union(mygenes_4, mygenes_8), rowMeans(counts(dds, normalized = TRUE)), decreasing = TRUE)[1:100]
heatmap.2(assay(rld)[select_sp, ], col = hmcol, Rowv = TRUE, Colv = TRUE, scale = c("row"), dendrogram="both", trace="none", margin=c(10,8), cexRow = 0.7, cexCol = 0.7)
```

<img src="schag1_files/figure-html/unnamed-chunk-11-5.png" style="display: block; margin: auto;" />

When clustering with `Hclust`, Artemia and 790ByPlus make a group while Serum and Spartina are in the other one. This is very similar to what was shown in the heatmaps above. 


```r
# Heatmap of sample-to-sample distances (uses all genes in the sample)
distsRL <- dist(t(assay(rld)))
mat <- as.matrix(distsRL)
heatmap.2(mat, trace="none", col = rev(hmcol_2), margin=c(13, 13), main = "Sample-to-sample distances (rlog)")
```

<img src="schag1_files/figure-html/unnamed-chunk-12-1.png" style="display: block; margin: auto;" />

```r
# Using the 1000 most highly expressed genes (selected by mean expression across samples):
select <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:1000]
distsRL <- dist(t(assay(rld)[select,]))
hc = hclust(distsRL)
plot(hc, main="Sample-to-sample distances, 1000 top genes (rlog)")
```

<img src="schag1_files/figure-html/unnamed-chunk-12-2.png" style="display: block; margin: auto;" />

```r
# Selecting genes based on variance (the varFilter in the genefilter package is especially designed for gene data)
m.var <- varFilter(assay(rld), var.func=IQR, var.cutoff=0.6, filterByQuantile=TRUE)
distsRL <- dist(t(m.var))
hc = hclust(distsRL)
plot(hc, main="Sample-to-sample distances, variable genes (rlog)")
```

<img src="schag1_files/figure-html/unnamed-chunk-12-3.png" style="display: block; margin: auto;" />

# Functional Analyses

- Enrichment Analysis 

The results below do not show any substantial up or down regulation pattern in any of the conditions. There are signals of transporters activity and electron transfer.


```bash

# First, parse the eggNOG-mapper annotations file to the tabular format GOstatsPlus needs
python ../convertGOstatsPlus.py Schag1_GeneCatalog_proteins_20121220_one_line.fasta.emapper.annotations Schag1_GeneCatalog_proteins_20121220.gostats

# Second, change the names to the protein IDs
awk 'BEGIN {OFS="\t"};  { split($1, a, "|"); print a[3], $2, $3 }' Schag1_GeneCatalog_proteins_20121220.gostats > Schag1_GeneCatalog_proteins_20121220.annot

```

After the annot file is done, the GOstatsPlus package can be used (GOstats, GOstatsPlus and GSEABase libraries are loaded at the beginning of the file).


```r
# Create the GOstats object and save it, in case that we want to re-run the sript. These are all the annotations available for Schag1 proteins.
if(file.exists("Schag1_gsc.rda")){
  load(file="Schag1_gsc.rda")
}else{
  fn_annot = "Schag1_GeneCatalog_proteins_20121220.annot"
  GO_gsc = b2g_to_gsc(file = fn_annot, organism = "Schag1")
  save(GO_gsc, file = "Schag1_gsc.rda")
}
```

- **Top expressed genes by condition**

The results below do not show any substantial up or down regulation pattern in any of the conditions. There are signals of biosynthesis of molecules, lipids and a lot of transporters.

* 790ByPlus 


```r
# Write genes on a file
write.table(as.character(mygenes_1), file = "upreg_790BP.txt", sep = "\t")
write.table(as.character(mygenes_5), file = "downreg_790BP.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_790BP.txt | grep -v "x" > upreg_790BP.list
cut -f2 downreg_790BP.txt | grep -v "x" > downreg_790BP.list

# Get the upregulated protein IDs
cat upreg_790BP.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_790BP_protID.list
wc -l upreg_790BP_protID.list

# Get the downregulated protein IDs
cat downreg_790BP.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_790BP_protID.list
wc -l downreg_790BP_protID.list
```


```r
upBP_tbl <- read.table(file = "upreg_790BP_protID.list")
downBP_tbl <- read.table(file = "downreg_790BP_protID.list")

upBP_IDs <- as.character(upBP_tbl$V1)
downBP_IDs <- as.character(downBP_tbl$V1)
```



```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upBP_BP <- test_GO(upBP_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upBP_MF <- test_GO(upBP_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upBP_CC <- test_GO(upBP_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downBP_BP <- test_GO(downBP_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downBP_MF <- test_GO(downBP_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downBP_CC <- test_GO(downBP_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upBP_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio   ExpCount Count Size
## 1  GO:0005242 0.002368756 43.290780 0.07893041     2    5
## 2  GO:0022843 0.002554212 13.217391 0.28414948     3   18
## 3  GO:0005244 0.004026823 11.003623 0.33150773     3   21
## 4  GO:0005261 0.004304013  6.874074 0.67880155     4   43
## 5  GO:0022832 0.004611245 10.421053 0.34729381     3   22
## 6  GO:0022838 0.005939558  6.226357 0.74194588     4   47
## 7  GO:0005216 0.005939558  6.226357 0.74194588     4   47
## 8  GO:0030552 0.006434374 21.624113 0.12628866     2    8
## 9  GO:0005249 0.006434374 21.624113 0.12628866     2    8
## 10 GO:0022803 0.007407905  5.814493 0.78930412     4   50
## 11 GO:0015267 0.007407905  5.814493 0.78930412     4   50
## 12 GO:0099094 0.008189727 18.528875 0.14207474     2    9
## 13 GO:0005267 0.012262556 14.401891 0.17364691     2   11
## 14 GO:0005096 0.013298205  6.805097 0.50515464     3   32
## 15 GO:0030551 0.014567836 12.957447 0.18943299     2   12
## 16 GO:0022839 0.015694183  6.361851 0.53672680     3   34
## 17 GO:0005534 0.015786082       Inf 0.01578608     1    1
## 18 GO:0004035 0.015786082       Inf 0.01578608     1    1
## 19 GO:0033862 0.015786082       Inf 0.01578608     1    1
## 20 GO:0035925 0.015786082       Inf 0.01578608     1    1
##                                           Term
## 1  inward rectifier potassium channel activity
## 2        voltage-gated cation channel activity
## 3           voltage-gated ion channel activity
## 4                      cation channel activity
## 5               voltage-gated channel activity
## 6          substrate-specific channel activity
## 7                         ion channel activity
## 8                                 cAMP binding
## 9     voltage-gated potassium channel activity
## 10  passive transmembrane transporter activity
## 11                            channel activity
## 12        ligand-gated cation channel activity
## 13                  potassium channel activity
## 14                   GTPase activator activity
## 15                   cyclic nucleotide binding
## 16                  ion gated channel activity
## 17                           galactose binding
## 18               alkaline phosphatase activity
## 19                         UMP kinase activity
## 20          mRNA 3'-UTR AU-rich region binding
```

```r
head(summary(downBP_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0015114 4.064587e-07       Inf 0.023195876     3    3
## 2  GO:0015321 5.731068e-05       Inf 0.015463918     2    2
## 3  GO:0005436 5.731068e-05       Inf 0.015463918     2    2
## 4  GO:0015103 3.630619e-04  27.35714 0.146907216     3   19
## 5  GO:1901677 4.932784e-04  24.30159 0.162371134     3   21
## 6  GO:0042301 8.435253e-04  69.90909 0.046391753     2    6
## 7  GO:0015296 1.175360e-03  55.90909 0.054123711     2    7
## 8  GO:0031402 2.483147e-03  34.90909 0.077319588     2   10
## 9  GO:0015293 2.483147e-03  34.90909 0.077319588     2   10
## 10 GO:0015294 2.483147e-03  34.90909 0.077319588     2   10
## 11 GO:0022853 3.327251e-03  11.74903 0.309278351     3   40
## 12 GO:0031420 5.658805e-03  21.44755 0.115979381     2   15
## 13 GO:0015081 7.260689e-03  18.57576 0.131443299     2   17
## 14 GO:0016158 7.731959e-03       Inf 0.007731959     1    1
## 15 GO:0047844 7.731959e-03       Inf 0.007731959     1    1
## 16 GO:0033819 7.731959e-03       Inf 0.007731959     1    1
## 17 GO:0052810 7.731959e-03       Inf 0.007731959     1    1
## 18 GO:0030507 7.731959e-03       Inf 0.007731959     1    1
## 19 GO:0017118 7.731959e-03       Inf 0.007731959     1    1
## 20 GO:0015415 7.731959e-03       Inf 0.007731959     1    1
##                                                               Term
## 1                 phosphate ion transmembrane transporter activity
## 2    sodium-dependent phosphate transmembrane transporter activity
## 3                              sodium:phosphate symporter activity
## 4               inorganic anion transmembrane transporter activity
## 5                     phosphate transmembrane transporter activity
## 6                                            phosphate ion binding
## 7                                  anion:cation symporter activity
## 8                                               sodium ion binding
## 9                                               symporter activity
## 10                                solute:cation symporter activity
## 11                   active ion transmembrane transporter activity
## 12                                        alkali metal ion binding
## 13                   sodium ion transmembrane transporter activity
## 14                                              3-phytase activity
## 15                                deoxycytidine deaminase activity
## 16                           lipoyl(octanoyl) transferase activity
## 17                        1-phosphatidylinositol-5-kinase activity
## 18                                                spectrin binding
## 19                                      lipoyltransferase activity
## 20 ATPase-coupled phosphate ion transmembrane transporter activity
```

* Artemia


```r
# Write genes on a file
write.table(as.character(mygenes_2), file = "upreg_Ar.txt", sep = "\t")
write.table(as.character(mygenes_6), file = "downreg_Ar.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Ar.txt | grep -v "x" > upreg_Ar.list
cut -f2 downreg_Ar.txt | grep -v "x" > downreg_Ar.list

# Get the upregulated protein IDs
cat upreg_Ar.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Ar_protID.list
wc -l upreg_Ar_protID.list

# Get the downregulated protein IDs
cat downreg_Ar.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Ar_protID.list
wc -l downreg_Ar_protID.list
```


```r
upAr_tbl <- read.table(file = "upreg_Ar_protID.list")
downAr_tbl <- read.table(file = "downreg_Ar_protID.list")

upAr_IDs <- as.character(upAr_tbl$V1)
downAr_IDs <- as.character(downAr_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upAr_BP <- test_GO(upAr_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upAr_MF <- test_GO(upAr_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upAr_CC <- test_GO(upAr_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downAr_BP <- test_GO(downAr_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downAr_MF <- test_GO(downAr_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downAr_CC <- test_GO(downAr_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upAr_MF), 20)
```

```
##        GOMFID       Pvalue  OddsRatio    ExpCount Count Size
## 1  GO:0015114 2.289343e-07        Inf 0.019329897     3    3
## 2  GO:0015321 3.945301e-05        Inf 0.012886598     2    2
## 3  GO:0005436 3.945301e-05        Inf 0.012886598     2    2
## 4  GO:0015103 2.076872e-04  33.838235 0.122422680     3   19
## 5  GO:1901677 2.827224e-04  30.058824 0.135309278     3   21
## 6  GO:0042301 5.826941e-04  85.555556 0.038659794     2    6
## 7  GO:0015296 8.126190e-04  68.422222 0.045103093     2    7
## 8  GO:0031402 1.721225e-03  42.722222 0.064432990     2   10
## 9  GO:0015293 1.721225e-03  42.722222 0.064432990     2   10
## 10 GO:0015294 1.721225e-03  42.722222 0.064432990     2   10
## 11 GO:0022853 1.942185e-03  14.532591 0.257731959     3   40
## 12 GO:0031420 3.939298e-03  26.247863 0.096649485     2   15
## 13 GO:0015081 5.063061e-03  22.733333 0.109536082     2   17
## 14 GO:0017178 6.443299e-03        Inf 0.006443299     1    1
## 15 GO:0015415 6.443299e-03        Inf 0.006443299     1    1
## 16 GO:0008942 6.443299e-03        Inf 0.006443299     1    1
## 17 GO:0098809 6.443299e-03        Inf 0.006443299     1    1
## 18 GO:0034017 6.443299e-03        Inf 0.006443299     1    1
## 19 GO:0008509 7.747258e-03   8.601518 0.418814433     3   65
## 20 GO:0000773 1.284714e-02 162.263158 0.012886598     1    2
##                                                               Term
## 1                 phosphate ion transmembrane transporter activity
## 2    sodium-dependent phosphate transmembrane transporter activity
## 3                              sodium:phosphate symporter activity
## 4               inorganic anion transmembrane transporter activity
## 5                     phosphate transmembrane transporter activity
## 6                                            phosphate ion binding
## 7                                  anion:cation symporter activity
## 8                                               sodium ion binding
## 9                                               symporter activity
## 10                                solute:cation symporter activity
## 11                   active ion transmembrane transporter activity
## 12                                        alkali metal ion binding
## 13                   sodium ion transmembrane transporter activity
## 14                               diphthine-ammonia ligase activity
## 15 ATPase-coupled phosphate ion transmembrane transporter activity
## 16                            nitrite reductase [NAD(P)H] activity
## 17                                      nitrite reductase activity
## 18        trans-2-decenoyl-acyl-carrier-protein isomerase activity
## 19                        anion transmembrane transporter activity
## 20  phosphatidyl-N-methylethanolamine N-methyltransferase activity
```

```r
head(summary(downAr_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0016708 0.002255155       Inf 0.002255155     1    1
## 2  GO:0008941 0.002255155       Inf 0.002255155     1    1
## 3  GO:0004090 0.006752389 257.91667 0.006765464     1    3
## 4  GO:0071997 0.006752389 257.91667 0.006765464     1    3
## 5  GO:0015431 0.006752389 257.91667 0.006765464     1    3
## 6  GO:0042937 0.006752389 257.91667 0.006765464     1    3
## 7  GO:0034634 0.006752389 257.91667 0.006765464     1    3
## 8  GO:0004409 0.008994483 171.88889 0.009020619     1    4
## 9  GO:0046624 0.008994483 171.88889 0.009020619     1    4
## 10 GO:0015198 0.011232237 128.87500 0.011275773     1    5
## 11 GO:0005324 0.011232237 128.87500 0.011275773     1    5
## 12 GO:0015245 0.011232237 128.87500 0.011275773     1    5
## 13 GO:0072349 0.011232237 128.87500 0.011275773     1    5
## 14 GO:0034040 0.011232237 128.87500 0.011275773     1    5
## 15 GO:0035673 0.011232237 128.87500 0.011275773     1    5
## 16 GO:0015562 0.013465659 103.06667 0.013530928     1    6
## 17 GO:0009881 0.015694756  85.86111 0.015786082     1    7
## 18 GO:0008020 0.015694756  85.86111 0.015786082     1    7
## 19 GO:0005546 0.022356161  57.18519 0.022551546     1   10
## 20 GO:0008559 0.024568025  51.45000 0.024806701     1   11
##                                                                                                                                                                                    Term
## 1  oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, NAD(P)H as one donor, and incorporation of two atoms of oxygen into one donor
## 2                                                                                                                                                     nitric oxide dioxygenase activity
## 3                                                                                                                                                   carbonyl reductase (NADPH) activity
## 4                                                                                                                                  glutathione S-conjugate-transporting ATPase activity
## 5                                                                                                                                     glutathione S-conjugate-exporting ATPase activity
## 6                                                                                                                                                       tripeptide transporter activity
## 7                                                                                                                                        glutathione transmembrane transporter activity
## 8                                                                                                                                                      homoaconitate hydratase activity
## 9                                                                                                                                                     sphingolipid transporter activity
## 10                                                                                                                                                    oligopeptide transporter activity
## 11                                                                                                                                           long-chain fatty acid transporter activity
## 12                                                                                                                                                      fatty acid transporter activity
## 13                                                                                                                               modified amino acid transmembrane transporter activity
## 14                                                                                                                                                   lipid-transporting ATPase activity
## 15                                                                                                                                      oligopeptide transmembrane transporter activity
## 16                                                                                                                                            efflux transmembrane transporter activity
## 17                                                                                                                                                               photoreceptor activity
## 18                                                                                                                                             G-protein coupled photoreceptor activity
## 19                                                                                                                                        phosphatidylinositol-4,5-bisphosphate binding
## 20                                                                                                                                              xenobiotic-transporting ATPase activity
```

* Serum 


```r
# Write genes on a file
write.table(as.character(mygenes_3), file = "upreg_Se.txt", sep = "\t")
write.table(as.character(mygenes_7), file = "downreg_Se.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Se.txt | grep -v "x" > upreg_Se.list
cut -f2 downreg_Se.txt | grep -v "x" > downreg_Se.list

# Get the upregulated protein IDs
cat upreg_Se.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Se_protID.list
wc -l upreg_Se_protID.list

# Get the downregulated protein IDs
cat downreg_Se.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Se_protID.list
wc -l downreg_Se_protID.list
```


```r
upSe_tbl <- read.table(file = "upreg_Se_protID.list")
downSe_tbl <- read.table(file = "downreg_Se_protID.list")

upSe_IDs <- as.character(upSe_tbl$V1)
downSe_IDs <- as.character(downSe_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upSe_BP <- test_GO(upSe_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upSe_MF <- test_GO(upSe_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upSe_CC <- test_GO(upSe_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downSe_BP <- test_GO(downSe_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downSe_MF <- test_GO(downSe_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downSe_CC <- test_GO(downSe_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```

```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upSe_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio   ExpCount Count Size
## 1  GO:0001047 7.606845e-05 64.914894 0.09664948     3    6
## 2  GO:0044212 1.731213e-04 11.590038 0.54768041     5   34
## 3  GO:0000975 1.731213e-04 11.590038 0.54768041     5   34
## 4  GO:0001067 1.731213e-04 11.590038 0.54768041     5   34
## 5  GO:0008138 3.521371e-04 14.666667 0.35438144     4   22
## 6  GO:0033549 5.927858e-04 24.303191 0.17719072     3   11
## 7  GO:0003677 1.081342e-03  3.747382 3.23775773    10  201
## 8  GO:0004016 1.494899e-03 63.583333 0.06443299     2    4
## 9  GO:0009703 1.494899e-03 63.583333 0.06443299     2    4
## 10 GO:0008940 1.494899e-03 63.583333 0.06443299     2    4
## 11 GO:0004383 2.465851e-03 42.375000 0.08054124     2    5
## 12 GO:0016661 2.465851e-03 42.375000 0.08054124     2    5
## 13 GO:0046857 2.465851e-03 42.375000 0.08054124     2    5
## 14 GO:0016849 3.660742e-03 31.770833 0.09664948     2    6
## 15 GO:0003700 4.845780e-03  4.294004 1.61082474     6  100
## 16 GO:0009975 5.072395e-03 25.408333 0.11275773     2    7
## 17 GO:0001071 5.089217e-03  4.247368 1.62693299     6  101
## 18 GO:0001098 6.693798e-03 21.166667 0.12886598     2    8
## 19 GO:0001099 6.693798e-03 21.166667 0.12886598     2    8
## 20 GO:0070063 8.518101e-03 18.136905 0.14497423     2    9
##                                                                                                      Term
## 1                                                                                   core promoter binding
## 2                                                             transcription regulatory region DNA binding
## 3                                                                           regulatory region DNA binding
## 4                                                                  regulatory region nucleic acid binding
## 5                                                  protein tyrosine/serine/threonine phosphatase activity
## 6                                                                         MAP kinase phosphatase activity
## 7                                                                                             DNA binding
## 8                                                                              adenylate cyclase activity
## 9                                                                       nitrate reductase (NADH) activity
## 10                                                                             nitrate reductase activity
## 11                                                                             guanylate cyclase activity
## 12                               oxidoreductase activity, acting on other nitrogenous compounds as donors
## 13 oxidoreductase activity, acting on other nitrogenous compounds as donors, with NAD or NADP as acceptor
## 14                                                                       phosphorus-oxygen lyase activity
## 15                                           transcription factor activity, sequence-specific DNA binding
## 16                                                                                       cyclase activity
## 17                                                     nucleic acid binding transcription factor activity
## 18                                                                  basal transcription machinery binding
## 19                                                basal RNA polymerase II transcription machinery binding
## 20                                                                                 RNA polymerase binding
```

```r
head(summary(downSe_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0071997 0.001932367 1550.0000 0.001932990     1    3
## 2  GO:0015431 0.001932367 1550.0000 0.001932990     1    3
## 3  GO:0042937 0.001932367 1550.0000 0.001932990     1    3
## 4  GO:0034634 0.001932367 1550.0000 0.001932990     1    3
## 5  GO:0015432 0.002576074 1033.0000 0.002577320     1    4
## 6  GO:0004409 0.002576074 1033.0000 0.002577320     1    4
## 7  GO:0046624 0.002576074 1033.0000 0.002577320     1    4
## 8  GO:0015198 0.003219573  774.5000 0.003221649     1    5
## 9  GO:0005324 0.003219573  774.5000 0.003221649     1    5
## 10 GO:0015125 0.003219573  774.5000 0.003221649     1    5
## 11 GO:0015245 0.003219573  774.5000 0.003221649     1    5
## 12 GO:0072349 0.003219573  774.5000 0.003221649     1    5
## 13 GO:0034040 0.003219573  774.5000 0.003221649     1    5
## 14 GO:0035673 0.003219573  774.5000 0.003221649     1    5
## 15 GO:0015562 0.003862865  619.4000 0.003865979     1    6
## 16 GO:0008028 0.004505949  516.0000 0.004510309     1    7
## 17 GO:1901618 0.005148825  442.1429 0.005154639     1    8
## 18 GO:0043225 0.005148825  442.1429 0.005154639     1    8
## 19 GO:0008559 0.007076208  309.2000 0.007087629     1   11
## 20 GO:0042910 0.007076208  309.2000 0.007087629     1   11
##                                                           Term
## 1         glutathione S-conjugate-transporting ATPase activity
## 2            glutathione S-conjugate-exporting ATPase activity
## 3                              tripeptide transporter activity
## 4               glutathione transmembrane transporter activity
## 5                          bile acid-exporting ATPase activity
## 6                             homoaconitate hydratase activity
## 7                            sphingolipid transporter activity
## 8                            oligopeptide transporter activity
## 9                   long-chain fatty acid transporter activity
## 10                bile acid transmembrane transporter activity
## 11                             fatty acid transporter activity
## 12      modified amino acid transmembrane transporter activity
## 13                          lipid-transporting ATPase activity
## 14             oligopeptide transmembrane transporter activity
## 15                   efflux transmembrane transporter activity
## 16      monocarboxylic acid transmembrane transporter activity
## 17 organic hydroxy compound transmembrane transporter activity
## 18     ATPase-coupled anion transmembrane transporter activity
## 19                     xenobiotic-transporting ATPase activity
## 20                             xenobiotic transporter activity
```

* Spartina


```r
# Write genes on a file
write.table(as.character(mygenes_4), file = "upreg_Sp.txt", sep = "\t")
write.table(as.character(mygenes_8), file = "downreg_Sp.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 upreg_Sp.txt | grep -v "x" > upreg_Sp.list
cut -f2 downreg_Sp.txt | grep -v "x" > downreg_Sp.list

# Get the upregulated protein IDs
cat upreg_Sp.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > upreg_Sp_protID.list
wc -l upreg_Sp_protID.list

# Get the downregulated protein IDs
cat downreg_Sp.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > downreg_Sp_protID.list
wc -l downreg_Sp_protID.list
```


```r
upSp_tbl <- read.table(file = "upreg_Sp_protID.list")
downSp_tbl <- read.table(file = "downreg_Sp_protID.list")

upSp_IDs <- as.character(upSp_tbl$V1)
downSp_IDs <- as.character(downSp_tbl$V1)
```


```r
# Do the enrichment analysis for Biological Process, Molecular Function and Cellular Component
upSp_BP <- test_GO(upSp_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
upSp_MF <- test_GO(upSp_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
upSp_CC <- test_GO(upSp_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)

downSp_BP <- test_GO(downSp_IDs, ontology = "BP", gsc=GO_gsc, pval = 0.05)
downSp_MF <- test_GO(downSp_IDs, ontology = "MF", gsc=GO_gsc, pval = 0.05)
downSp_CC <- test_GO(downSp_IDs, ontology = "CC", gsc=GO_gsc, pval = 0.05)
```


```r
# Focusing on the top 20 more significant of Molecular functions
head(summary(upSp_MF), 20)
```

```
##        GOMFID      Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0016705 0.000162026  42.60000 0.118556701     3   46
## 2  GO:0035240 0.002577320       Inf 0.002577320     1    1
## 3  GO:1901338 0.002577320       Inf 0.002577320     1    1
## 4  GO:0004511 0.002577320       Inf 0.002577320     1    1
## 5  GO:0004497 0.002604164  35.25287 0.079896907     2   31
## 6  GO:0046966 0.005148825 442.14286 0.005154639     1    2
## 7  GO:0070405 0.005148825 442.14286 0.005154639     1    2
## 8  GO:0034617 0.005148825 442.14286 0.005154639     1    2
## 9  GO:0003904 0.005148825 442.14286 0.005154639     1    2
## 10 GO:0004500 0.005148825 442.14286 0.005154639     1    2
## 11 GO:0008253 0.007714528 221.00000 0.007731959     1    3
## 12 GO:0016714 0.007714528 221.00000 0.007731959     1    3
## 13 GO:0016715 0.007714528 221.00000 0.007731959     1    3
## 14 GO:0051864 0.007714528 221.00000 0.007731959     1    3
## 15 GO:0003913 0.007714528 221.00000 0.007731959     1    3
## 16 GO:0008252 0.010274439 147.28571 0.010309278     1    4
## 17 GO:0016922 0.010274439 147.28571 0.010309278     1    4
## 18 GO:0008199 0.010274439 147.28571 0.010309278     1    4
## 19 GO:0019825 0.012828569 110.42857 0.012886598     1    5
## 20 GO:0032452 0.017919533  73.57143 0.018041237     1    7
##                                                                                                                                                                              Term
## 1                                                                           oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen
## 2                                                                                                                                                                dopamine binding
## 3                                                                                                                                                           catecholamine binding
## 4                                                                                                                                               tyrosine 3-monooxygenase activity
## 5                                                                                                                                                          monooxygenase activity
## 6                                                                                                                                                thyroid hormone receptor binding
## 7                                                                                                                                                            ammonium ion binding
## 8                                                                                                                                                     tetrahydrobiopterin binding
## 9                                                                                                                                      deoxyribodipyrimidine photo-lyase activity
## 10                                                                                                                                           dopamine beta-monooxygenase activity
## 11                                                                                                                                                       5'-nucleotidase activity
## 12 oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, reduced pteridine as one donor, and incorporation of one atom of oxygen
## 13 oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, reduced ascorbate as one donor, and incorporation of one atom of oxygen
## 14                                                                                                                                 histone demethylase activity (H3-K36 specific)
## 15                                                                                                                                                        DNA photolyase activity
## 16                                                                                                                                                          nucleotidase activity
## 17                                                                                                                                      ligand-dependent nuclear receptor binding
## 18                                                                                                                                                            ferric iron binding
## 19                                                                                                                                                                 oxygen binding
## 20                                                                                                                                                   histone demethylase activity
```

```r
head(summary(downSp_MF), 20)
```

```
##        GOMFID       Pvalue OddsRatio    ExpCount Count Size
## 1  GO:0016151 0.0002038095 154.40000 0.023195876     2    6
## 2  GO:0008273 0.0002038095 154.40000 0.023195876     2    6
## 3  GO:0022821 0.0002847201 123.48000 0.027061856     2    7
## 4  GO:0015368 0.0003788111 102.86667 0.030927835     2    8
## 5  GO:0046870 0.0003788111 102.86667 0.030927835     2    8
## 6  GO:0031402 0.0006061900  77.10000 0.038659794     2   10
## 7  GO:0030955 0.0006061900  77.10000 0.038659794     2   10
## 8  GO:0015491 0.0008852621  61.64000 0.046391753     2   12
## 9  GO:0015298 0.0008852621  61.64000 0.046391753     2   12
## 10 GO:0031420 0.0013993136  47.36923 0.057989691     2   15
## 11 GO:0015081 0.0018046653  41.02667 0.065721649     2   17
## 12 GO:0099516 0.0020258871  38.45000 0.069587629     2   18
## 13 GO:0015079 0.0027627490  32.34737 0.081185567     2   21
## 14 GO:0015297 0.0027627490  32.34737 0.081185567     2   21
## 15 GO:0005262 0.0027627490  32.34737 0.081185567     2   21
## 16 GO:0030145 0.0030324963  30.72000 0.085051546     2   22
## 17 GO:0047865 0.0038659794       Inf 0.003865979     1    1
## 18 GO:0015085 0.0045585503  24.53600 0.104381443     2   27
## 19 GO:0005509 0.0053019535  10.63121 0.375000000     3   97
## 20 GO:0046997 0.0077182540 281.00000 0.007731959     1    2
##                                                                                Term
## 1                                                             nickel cation binding
## 2                                     calcium, potassium:sodium antiporter activity
## 3                                                 potassium ion antiporter activity
## 4                                                calcium:cation antiporter activity
## 5                                                               cadmium ion binding
## 6                                                                sodium ion binding
## 7                                                             potassium ion binding
## 8                                                 cation:cation antiporter activity
## 9                                                 solute:cation antiporter activity
## 10                                                         alkali metal ion binding
## 11                                    sodium ion transmembrane transporter activity
## 12                                                          ion antiporter activity
## 13                                 potassium ion transmembrane transporter activity
## 14                                                              antiporter activity
## 15                                                         calcium channel activity
## 16                                                            manganese ion binding
## 17                                           dimethylglycine dehydrogenase activity
## 18                                   calcium ion transmembrane transporter activity
## 19                                                              calcium ion binding
## 20 oxidoreductase activity, acting on the CH-NH group of donors, flavin as acceptor
```

- **Top 50 highly expressed genes across samples**

From the results above regarding the top 50 highly expressed genes across samples. Thus, the annotation for those genes can be extracted from the catalogs. 


```r
# Get the top 50 genes from previously selections 
top_50_HEG <- rownames(assay(rld)[select_2, ])

# Write the genes on a file
write.table(as.character(top_50_HEG), file = "top50.txt", sep = "\t")
```


```bash
# Remove the headers
cut -f2 top50.txt | grep -v "x" > top50.list

# Get the downregulated protein IDs
cat top50.list | while read line ; do egrep "gene_id "+$line Schag1.nd.gtf | egrep "transcript\b" | egrep "gene_name" | cut -f3 -d ";" | cut -f3 -d "|" | cut -f1 -d '"' ; done | sort | uniq > top50_protID.list
wc -l top50_protID.list

# Get the GO terms from the catalog
cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Schag1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "biological_process" | cut -f3 | sort | uniq -c > top50_GO_BP.list

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Schag1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "molecular_function" | cut -f3 | sort | uniq -c > top50_GO_MF.list

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Schag1_GeneCatalog_proteins_20121220_GO.tab ; done | egrep "cellular_component" | cut -f3 | sort | uniq -c > top50_GO_CC.list

# Get the KEGG annotations from the catalog

cat top50_protID.list | while read line; do awk -F"\t" -v line=$line 'BEGIN {OFS="\t"} ; { if ( $1 == line ) print $0 }' Schag1_GeneCatalog_proteins_20121220_KEGG.tab ; done | cut -f7 | egrep -v "\N" | sort | uniq -c > top50_KEGG.list
```


```r
# Writing the tables and data frames
Top50_GO_MF_table <- read.table(file="top50_GO_MF.list", sep = "\t", header=FALSE)
colnames(Top50_GO_MF_table) <- "Molecular Function"
Top50_GO_MF_df <- as.data.frame(Top50_GO_MF_table)

Top50_KEGG_table <- read.table(file="top50_KEGG.list", sep = "\t", header=FALSE)
colnames(Top50_KEGG_table) <- "KEGG pathways"
Top50_KEGG_df <- as.data.frame(Top50_KEGG_table)

# Making the tables
pandoc.table(Top50_GO_MF_df, justify = c("left"), split.table = Inf, caption = "Annotation of Molecular Function GO terms for TOP50 highly expressed genes", style = "rmarkdown")
```



| Molecular Function                                                  |
|:--------------------------------------------------------------------|
| 4 ATP binding                                                       |
| 1 ATP-dependent DNA helicase activity                               |
| 1 ATP-dependent RNA helicase activity                               |
| 3 ATP-dependent helicase activity                                   |
| 1 ATPase activity                                                   |
| 1 ATPase activity, coupled                                          |
| 1 ATPase activity, coupled to transmembrane movement of ions        |
| 1 ATPase activity, coupled to transmembrane movement of substances  |
| 1 ATPase activity, uncoupled                                        |
| 1 DNA-dependent ATPase activity                                     |
| 1 DNA-directed RNA polymerase I activity                            |
| 1 DNA-directed RNA polymerase II activity                           |
| 1 DNA-directed RNA polymerase III activity                          |
| 1 DNA-directed RNA polymerase activity                              |
| 2 GTP binding                                                       |
| 1 GTPase activity                                                   |
| 1 RNA binding                                                       |
| 1 RNA polymerase activity                                           |
| 1 RNA-dependent ATPase activity                                     |
| 4 calcium ion binding                                               |
| 1 carbonate dehydratase activity                                    |
| 1 copper chaperone activity                                         |
| 1 copper ion binding                                                |
| 1 dynein ATPase activity                                            |
| 1 glutathione peroxidase activity                                   |
| 1 hydrolase activity                                                |
| 1 microtubule motor activity                                        |
| 2 nucleic acid binding                                              |
| 1 nucleoside-triphosphatase activity                                |
| 1 nucleotide binding                                                |
| 1 proteasome endopeptidase activity                                 |
| 1 protein binding                                                   |
| 1 protein disulfide isomerase activity                              |
| 1 protein-transmembrane transporting ATPase activity                |
| 1 single-stranded DNA-dependent ATP-dependent DNA helicase activity |
| 5 structural constituent of ribosome                                |
| 1 structural molecule activity                                      |
| 1 threonine endopeptidase activity                                  |
| 1 translation initiation factor activity                            |
| 1 unfolded protein binding                                          |
| 2 zinc ion binding                                                  |

Table: Annotation of Molecular Function GO terms for TOP50 highly expressed genes

```r
pandoc.table(Top50_KEGG_df, justify = c("left"), split.table = Inf, caption = "KEGG annotations for TOP50 highly expressed genes", style = "rmarkdown")
```



| KEGG pathways            |
|:-------------------------|
| 1 Glutathione metabolism |
| 2 Purine metabolism      |
| 1 Pyrimidine metabolism  |

Table: KEGG annotations for TOP50 highly expressed genes

