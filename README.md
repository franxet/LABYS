# Labyrinthulomycetes
Latorre F.  



# INTRODUCTION

The **Labyrinthulomycetes** are a class of protists that produce a network of filaments or tubes which serve as tracks for the cells to glide along and absorb nutrients for them. The two main groups are the *labyrinthulids* (or slime nets) and *thraustochytrids*. They are mostly marine, commonly found as parasites on algae and seagrasses or as decomposers on dead plant material.

Transcriptomic data for *Aplanochytrium kergulense* (Aplke1), *Aurantiochytrium limacinum* (Aurli1) and *Schizochytrium aggregatum* (Schag1) are available thorugh the **Moore Foundation Marine Microbial Eukaryote Transcriptome Sequencing Project** (MMETSP). All of the three species has been cultured in four different standard conditions. 


| **Medium**        |**Aditions**                                    | **Main Characteristics**                                                         |
|:------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------|
| ATCC 790BY+       | 0.1% yeast extract, 0.1% glucose, 0.1% peptone | Very rich and complex. Mixture of water soluble vitamins, amino acids, peptides. |
| Artemia detritus  | 0.125% freeze dtried Artemia                   | Quitine, proteins, lipids.                                                       |
| Horse serum       | 1% horse serum                                 | Very complex, not as rich as 790BY+                                              |
| Spartina serum    | 0.125% spartina detritus                       | Cellulose, lignine, carbohydrates                                                |

The aim of this project is to study which genes are differentialy expressed regarding the mentioned conditions while trying to give ecological meaning to 
them. Thus, `DESeq2` package from Bioconductor will be used to assess this issue. 

# METHODS

Here is a summary of the methods followed for all three species. Read this before going to the results section in order to understand them. 

- **Raw counts**

Counts for the differential analysis expression with `DESeq2` have been obtained from `HTSeq-count`. The bam files and gtf used are from the **HiSat2 - StringTie - Ballgown** pipeline. These results are orientative, for the final paper new alignments and gtf files should be made. 

- **Setting the environment, building DESeq2 parameters**

Using the raw counts from HTSeq, the next step is to build all the parameters for using DESeq2, that is the DESeq2 object. DESeq2 allows the user to work without replicates by treating all the samples as replicates and thus estimating the dispersion. DESeq2 developers advise on using real replicates to asses differential expression, as the statistical power of this approach loses validity without them. 

- **Inspecting the samples**

First step is to check whether the samples have strange patterns in expression or if they can be clustered together in some way. For this, boxplots and distribution graphs are plotted.

- **DESeq2 results**

To see the `DESeq()` results, we must do it in pairs; one condition against the others. This will yield a results table where each gene in condition A is compared against condition B where the log2 fold change is computed. Negative values imply **down-regulation** in condition A when compared to B, and positive values are the **up-regulated** ones. Thus, in order to compare all the genes in different conditions, they must use the same condition as *control*. 

For each comparison, the top up-regulated and down-regulated genes can be estimated easily. Furthermore, for each of the four conditions taken as control, three different comparison are possible. At the end, twenty-four pair-wise comparison are made (2 x 3 x 4). To sum up all the results, the names of the top up-regulated and down-regulated genes are extracted and plotted in a Venn Diagram. Those genes that are present in all the three comparisons when a condition is taken as control (intersection of the diagram), can be of relevance in our study. Because `DESeq()` is working without replicates, the adjusted p-value cannot be taken into consideration.

The idea is to find which genes are more and less expressed for each condiction thorugh the intersections, as they may be an indicator of different adaptation patterns for different food sources or needs.  

  - Regularized log transformation
  
DESeq2 suggests to use the regularized log transformation for RNAseq data, which should give the best results in this case. However, because we still do not have replicates, the same problems as before are faced. 
  
First step is to plot in a heatmap the top 50 most highly expressed genes across samples and see the expression patterns. After that, the same is done with the Venn Diagrams: first the top 50 more expressed genes within all the intersections and later on, all of them individually. This will show the patterns of expression for each condicition and will help decide which genes are worth studying.
  
Also, a clustering using mean expression values across samples is done just to see if there is any pattern that can be helpfull. For example, condition A and C show similar expression values for the same genes. May be indicating that the same pathways are more expressed in these conditions than the others. 
  
  - Using 'fake' replicates
  
As a test, just for *Aplke1*, the results from the clustering are used to generate two pairs of conditions and treat them as replicates for DESeq2 to be able to compute correctly the dispersion and give meaningful p-values. The process is exactly the same as before, just modeling the results by new conditions (rich vs specific mediums). The results are not exactly real, as the conditions grouped together are not exactly replicates, but this way significant p-values are obtained.
  
To test if this approach is valid, the Differential Expressed genes (DEgenes) from this fake replicates approach are compared to the ones obtained from the intersections in number. How many DEgenes are in the intersections?  

- **Functional analyses**

For the functional analyses of the genes of interest, the already catalogs for KEGG and GO terms were used. Because the date of this annotations are from 2012, a new annotation is done using the new [eggNOG-mapper](http://eggnogdb.embl.de/#/app/emapper), which returns up-to-date GO terms and KEGG IDs.

One of the approaches to use with GO terms is an **Enrichment Analysis**; where instead of looking at genes, we try to infere if certain biological processes or molecular functions are up or down regulated using the genes that are differential expressed within those areas. In other words, if, for example, a biological process consists of 20 genes and 18 of them are found to be up-regulated, it is safe to say that this biological process is up-regulated.

There are several tools to do this when the target is a model organism with genetic information available in different databases. However, none of the ones treated here are one of them. Thus, we need an alternative R package to do it. [GOstatsPlus](https://github.com/davfre/GOstatsPlus) is a straight forward R package designed to use **GOstats** for a non-model organism. This package requires of an annotation file in the format of "GeneID GOid Description". From the file delivered by eggNOG-mapper, this annotation file can be made. A little custom python script can do the trick (see for convertGOstatsPlus.py in the git folder).

The main purpose of this step is to find annoptations that explains the behavior of this organism when cultivated in the four different conditions.

  - 'Rich' vs 'Specific' medium 

Just for *Aplke1*, GOterms are checked from the catalogs and enrichment analysis is done with GOstatsPlus.

  - Top expressed genes by condition

Enrichment analysis with the three species of interest for the up and down regulated genes for each condition from the intersections of Venn Diagrams.

  - Top 50 highly expressed genes across samples

Check the KEGG and GO terms for the top 50 highly expressed genes from the already done catalogs. 


# RESULTS 

I recommend to see the results for Aplke1 first, since it is the most detailed one and the other two species follow the patterns used in it. 

The results of differential expression can be found for the three different species here:

* [Aplke1](./Aplke1/aplke1.md)
* [Aurli1](./Aurli1/aurli1.md)
* [Schag1](./Schag1/schag1.md)

# R-SESSION


```r
sessionInfo()
```

```
## R version 3.4.0 (2017-04-21)
## Platform: x86_64-apple-darwin15.6.0 (64-bit)
## Running under: macOS Sierra 10.12.6
## 
## Matrix products: default
## BLAS: /Library/Frameworks/R.framework/Versions/3.4/Resources/lib/libRblas.0.dylib
## LAPACK: /Library/Frameworks/R.framework/Versions/3.4/Resources/lib/libRlapack.dylib
## 
## locale:
## [1] es_ES.UTF-8/es_ES.UTF-8/es_ES.UTF-8/C/es_ES.UTF-8/es_ES.UTF-8
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## loaded via a namespace (and not attached):
##  [1] compiler_3.4.0  backports_1.1.0 magrittr_1.5    rprojroot_1.2  
##  [5] tools_3.4.0     htmltools_0.3.6 yaml_2.1.14     Rcpp_0.12.11   
##  [9] stringi_1.1.5   rmarkdown_1.5   knitr_1.16      stringr_1.2.0  
## [13] digest_0.6.12   evaluate_0.10
```

